include "TriggerLibs/NativeLib"
include "TriggerLibs/HeroesLib"
include "TriggerLibs/GameLib"
include "TriggerLibs/AILib"
include "TriggerLibs/UILib"

include "LibHAMA_h"

//--------------------------------------------------------------------------------------------------
// Library: Amazon
//--------------------------------------------------------------------------------------------------
// External Library Initialization
void libHAMA_InitLibraries () {
    libNtve_InitVariables();
    libCore_InitVariables();
    libGame_InitVariables();
    libAIAI_InitVariables();
    libUIUI_InitVariables();
}

// Variable Initialization
bool libHAMA_InitVariables_completed = false;

void libHAMA_InitVariables () {
    if (libHAMA_InitVariables_completed) {
        return;
    }

    libHAMA_InitVariables_completed = true;

    libHAMA_gv_amazonUI.lv_mainPanel = c_invalidDialogControlId;
    libHAMA_gv_amazonUI.lv_SurgeOfLightProgressBar = c_invalidDialogControlId;
    libHAMA_gv_amazon_HeroSpecificCommandCardPanel = c_invalidDialogControlId;
    libHAMA_gv_amazon_Button05OverridePanel = c_invalidDialogControlId;
}

// Custom Script
//--------------------------------------------------------------------------------------------------
// Custom Script: #Include Amazon Tactical AI
//--------------------------------------------------------------------------------------------------
include "StormTactical.galaxy"
include "AmazonTactical.galaxy"

void libHAMA_InitCustomScript () {
}

// Functions
void libHAMA_gf_HeroAmazonIncrementHeroCountFunction () {
    // Implementation
    libHAMA_gv_heroAmazonTriggerRegistrationVariable += 1;
    if ((libHAMA_gv_heroAmazonTriggerRegistrationVariable == 1)) {
        TriggerEnable(libHAMA_gt_HeroAmazonValkyrieCheckForStuckHero, true);
        TriggerEnable(libHAMA_gt_HeroAmazonSurgeofLightDamageTaken, true);
        TriggerEnable(libHAMA_gt_HeroAmazonSurgeofLightResetCharge, true);
        TriggerEnable(libHAMA_gt_HeroAmazonInnerLightCheckforCC, true);
    }

}

void libHAMA_gf_HeroAmazonDecrementHeroCountFunction () {
    // Implementation
    libHAMA_gv_heroAmazonTriggerRegistrationVariable -= 1;
    if ((libHAMA_gv_heroAmazonTriggerRegistrationVariable < 0)) {
        libHAMA_gv_heroAmazonTriggerRegistrationVariable = 0;
    }

    if ((libHAMA_gv_heroAmazonTriggerRegistrationVariable == 0)) {
        TriggerEnable(libHAMA_gt_HeroAmazonValkyrieCheckForStuckHero, false);
        TriggerEnable(libHAMA_gt_HeroAmazonSurgeofLightDamageTaken, false);
        TriggerEnable(libHAMA_gt_HeroAmazonSurgeofLightResetCharge, false);
        TriggerEnable(libHAMA_gt_HeroAmazonInnerLightCheckforCC, false);
    }

}

// Triggers
//--------------------------------------------------------------------------------------------------
// Trigger: Amazon - Hero Specific Command Card Panel Initialize
//--------------------------------------------------------------------------------------------------
bool libHAMA_gt_AmazonHeroSpecificCommandCardPanelInitialize_Func (bool testConds, bool runActions) {
    // Actions
    if (!runActions) {
        return true;
    }

    DialogControlHookupStandard(c_triggerControlTypePanel, "UIContainer/ConsoleUIContainer/HeroSpecificCommandCardPanel");
    libUIUI_gf_UISetDialogItemToSelectedOrObserved(DialogControlLastCreated());
    libHAMA_gv_amazon_HeroSpecificCommandCardPanel = DialogControlLastCreated();
    DialogControlHookupStandard(c_triggerControlTypePanel, "UIContainer/ConsoleUIContainer/CommandPanel/CommandButton05/HeroSpecificCommandCardPanel");
    libUIUI_gf_UISetDialogItemToSelectedOrObserved(DialogControlLastCreated());
    libHAMA_gv_amazon_Button05OverridePanel = DialogControlLastCreated();
    return true;
}

//--------------------------------------------------------------------------------------------------
void libHAMA_gt_AmazonHeroSpecificCommandCardPanelInitialize_Init () {
    libHAMA_gt_AmazonHeroSpecificCommandCardPanelInitialize = TriggerCreate("libHAMA_gt_AmazonHeroSpecificCommandCardPanelInitialize_Func");
    libCore_gf_IncludeModInitialization(libHAMA_gt_AmazonHeroSpecificCommandCardPanelInitialize);
}

//--------------------------------------------------------------------------------------------------
// Trigger: Amazon - UI Initialize
//--------------------------------------------------------------------------------------------------
bool libHAMA_gt_AmazonUIInitialize_Func (bool testConds, bool runActions) {
    int init_i;

    // Variable Declarations
    int[6] lv_panels;
    int lv_currentPanelLayer;

    // Variable Initialization
    for (init_i = 0; init_i <= 5; init_i += 1) {
        lv_panels[init_i] = c_invalidDialogControlId;
    }

    // Actions
    if (!runActions) {
        return true;
    }

    DialogControlHookup(libHAMA_gv_amazon_HeroSpecificCommandCardPanel, c_triggerControlTypePanel, "AmazonCommandCardFrame");
    libHAMA_gv_amazonUI.lv_mainPanel = DialogControlLastCreated();
    lv_currentPanelLayer += 1;
    lv_panels[lv_currentPanelLayer] = DialogControlLastCreated();
    DialogControlHookup(libHAMA_gv_amazon_Button05OverridePanel, c_triggerControlTypePanel, "AmazonSurgeOfLightButtonOverrideFrame");
    lv_currentPanelLayer += 1;
    lv_panels[lv_currentPanelLayer] = DialogControlLastCreated();
    DialogControlHookup(lv_panels[lv_currentPanelLayer], c_triggerControlTypeProgressBar, "UnitStatusFrame0/SurgeOfLightProgressBar");
    libHAMA_gv_amazonUI.lv_SurgeOfLightProgressBar = DialogControlLastCreated();
    return true;
}

//--------------------------------------------------------------------------------------------------
void libHAMA_gt_AmazonUIInitialize_Init () {
    libHAMA_gt_AmazonUIInitialize = TriggerCreate("libHAMA_gt_AmazonUIInitialize_Func");
    libCore_gf_IncludeModInitialization(libHAMA_gt_AmazonUIInitialize);
}

//--------------------------------------------------------------------------------------------------
// Trigger: HeroAI - Misc Mod Initialization
//--------------------------------------------------------------------------------------------------
bool libHAMA_gt_HeroAIMiscModInitialization_Func (bool testConds, bool runActions) {
    // Actions
    if (!runActions) {
        return true;
    }

    libCore_gf_RegisterSegregationTrigger(libCore_ge_SegregationTriggerTypes_HeroAIInitializeHeroTierData, libHAMA_gt_HeroAIAddToTierDataAmazon);
    libCore_gf_RegisterSegregationTrigger(libCore_ge_SegregationTriggerTypes_HeroAIInitAbilities, libHAMA_gt_HeroAIInitAbilitiesAmazon);
    libAIAI_gf_HeroAIRegisterAOEwithWatchTrigger("AmazonFendChannelSearchArea", 3.75, 0.25, 3.75, false, true);
    libAIAI_gf_HeroAIRegisterAOEwithWatchTrigger("AmazonBallLightningImpactSet", 6.0, 1.5, 0.0, true, false);
    libAIAI_gf_HeroAIRegisterAOEwithWatchTrigger("AmazonBallLightningBounceToCasterImpactSet", 6.0, 1.5, 0.0, true, false);
    libAIAI_gf_HeroAIRegisterAOEwithWatchTrigger("AmazonBallLightningBounceToEnemyImpactSet", 6.0, 1.5, 0.0, true, false);
    return true;
}

//--------------------------------------------------------------------------------------------------
void libHAMA_gt_HeroAIMiscModInitialization_Init () {
    libHAMA_gt_HeroAIMiscModInitialization = TriggerCreate("libHAMA_gt_HeroAIMiscModInitialization_Func");
    TriggerAddEventMapInit(libHAMA_gt_HeroAIMiscModInitialization);
}

//--------------------------------------------------------------------------------------------------
// Trigger: HeroAI - Add To Tier Data - Amazon
//--------------------------------------------------------------------------------------------------
bool libHAMA_gt_HeroAIAddToTierDataAmazon_Func (bool testConds, bool runActions) {
    // Actions
    if (!runActions) {
        return true;
    }

    libAIAI_gf_HeroAIAddHeroToTierData(libAIAI_ge_HeroAIHeroTiers_TierHigh, "Amazon");
    return true;
}

//--------------------------------------------------------------------------------------------------
void libHAMA_gt_HeroAIAddToTierDataAmazon_Init () {
    libHAMA_gt_HeroAIAddToTierDataAmazon = TriggerCreate("libHAMA_gt_HeroAIAddToTierDataAmazon_Func");
}

//--------------------------------------------------------------------------------------------------
// Trigger: HeroAI - Init Abilities - Amazon
//--------------------------------------------------------------------------------------------------
bool libHAMA_gt_HeroAIInitAbilitiesAmazon_Func (bool testConds, bool runActions) {
    // Actions
    if (!runActions) {
        return true;
    }

    if ((UnitGetType(libCore_gv_segTriggerUnit) == "HeroAmazon")) {
        libAIAI_gv_heroAIInitAbilitesHeroFound = true;
        libAIAI_gv_aIHeroes[libCore_gv_segTriggerPlayer].lv_hasTactical = true;
        libAIAI_gv_aIHeroes[libCore_gv_segTriggerPlayer].lv_hasMount = true;
        libAIAI_gv_aIHeroes[libCore_gv_segTriggerPlayer].lv_castAbility[0] = AbilityCommand("AmazonLightningFury", 0);
        libAIAI_gv_aIHeroes[libCore_gv_segTriggerPlayer].lv_abilityType[0] = libAIAI_ge_HeroAISpellType_TargetEnemy;
        libAIAI_gv_aIHeroes[libCore_gv_segTriggerPlayer].lv_abilityRange[0] = 8.0;
        libAIAI_gv_aIHeroes[libCore_gv_segTriggerPlayer].lv_castAbility[1] = AbilityCommand("AmazonBlindingLight", 0);
        libAIAI_gv_aIHeroes[libCore_gv_segTriggerPlayer].lv_abilityType[1] = libAIAI_ge_HeroAISpellType_TargetEnemy;
        libAIAI_gv_aIHeroes[libCore_gv_segTriggerPlayer].lv_abilityRange[1] = 7.0;
    }

    return true;
}

//--------------------------------------------------------------------------------------------------
void libHAMA_gt_HeroAIInitAbilitiesAmazon_Init () {
    libHAMA_gt_HeroAIInitAbilitiesAmazon = TriggerCreate("libHAMA_gt_HeroAIInitAbilitiesAmazon_Func");
}

//--------------------------------------------------------------------------------------------------
// Trigger: Hero - Amazon - Valkyrie - Check For Stuck Hero
//--------------------------------------------------------------------------------------------------
bool libHAMA_gt_HeroAmazonValkyrieCheckForStuckHero_Func (bool testConds, bool runActions) {
    // Variable Declarations
    unit lv_impaledUnit;

    // Variable Initialization
    lv_impaledUnit = EventUnit();

    // Actions
    if (!runActions) {
        return true;
    }

    while ((UnitHasBehavior2(lv_impaledUnit, "AmazonValkyrieImpaleImpaledTarget") == true)) {
        if ((UnitGetPropertyFixed(lv_impaledUnit, c_unitPropMovementSpeedCurrent, c_unitPropCurrent) < libHAMA_gv_heroAmazonValkyrieMinimumMoveSpeed) && (UnitHasBehavior2(lv_impaledUnit, "AmazonValkyrieImpaleAccelerating") == false)) {
            UnitBehaviorRemove(lv_impaledUnit, "AmazonValkyrieImpaleImpaledTarget", 1);
        }

        Wait(0.0625, c_timeGame);
    }
    return true;
}

//--------------------------------------------------------------------------------------------------
void libHAMA_gt_HeroAmazonValkyrieCheckForStuckHero_Init () {
    libHAMA_gt_HeroAmazonValkyrieCheckForStuckHero = TriggerCreate("libHAMA_gt_HeroAmazonValkyrieCheckForStuckHero_Func");
    TriggerEnable(libHAMA_gt_HeroAmazonValkyrieCheckForStuckHero, false);
    TriggerAddEventUnitBehaviorChange(libHAMA_gt_HeroAmazonValkyrieCheckForStuckHero, null, "AmazonValkyrieImpaleImpaledTarget", c_unitBehaviorChangeCreate);
}

//--------------------------------------------------------------------------------------------------
// Trigger: Hero - Amazon - Surge of Light - Damage Taken
//--------------------------------------------------------------------------------------------------
bool libHAMA_gt_HeroAmazonSurgeofLightDamageTaken_Func (bool testConds, bool runActions) {
    // Variable Declarations
    unit lv_amazonUnit;
    int lv_amazonPlayer;
    string lv_maximumChargeReference;
    string lv_currentChargeReference;
    fixed lv_maximumCharge;
    fixed lv_currentCharge;

    // Variable Initialization
    lv_amazonUnit = EventUnit();
    lv_amazonPlayer = EventPlayer();
    lv_maximumChargeReference = "AmazonAvoidanceSurgeOfLightDummyMaximumCharge";
    lv_currentChargeReference = "AmazonAvoidanceSurgeOfLightDummyCurrentCharge";
    lv_maximumCharge = CatalogFieldValueGetAsFixed(c_gameCatalogEffect, lv_maximumChargeReference, "Amount", lv_amazonPlayer);
    lv_currentCharge = CatalogFieldValueGetAsFixed(c_gameCatalogEffect, lv_currentChargeReference, "Amount", lv_amazonPlayer);

    // Conditions
    if (testConds) {
        if (!((UnitHasBehavior2(lv_amazonUnit, "AmazonAvoidanceDamageReduction") == true))) {
            return false;
        }

        if (!((PlayerHasTalent(lv_amazonPlayer, "AmazonSurgeOfLight") == true))) {
            return false;
        }
    }

    // Actions
    if (!runActions) {
        return true;
    }

    if ((lv_currentCharge < lv_maximumCharge)) {
        CatalogFieldValueModifyFixed(c_gameCatalogEffect, lv_currentChargeReference, "Amount", lv_amazonPlayer, EventUnitDamageAmount(), c_upgradeOperationAdd);
        lv_currentCharge = CatalogFieldValueGetAsFixed(c_gameCatalogEffect, lv_currentChargeReference, "Amount", lv_amazonPlayer);
        libNtve_gf_SetDialogItemCurrentValue(libHAMA_gv_amazonUI.lv_SurgeOfLightProgressBar, FixedToInt((100.0 * (lv_currentCharge / lv_maximumCharge))), libCore_gv_playerGroupFromPlayer[lv_amazonPlayer]);
    }

    if ((lv_currentCharge >= lv_maximumCharge)) {
        UnitBehaviorAdd(lv_amazonUnit, "AmazonAvoidanceSurgeOfLightAbilityReady", lv_amazonUnit, 1);
    }

    return true;
}

//--------------------------------------------------------------------------------------------------
void libHAMA_gt_HeroAmazonSurgeofLightDamageTaken_Init () {
    libHAMA_gt_HeroAmazonSurgeofLightDamageTaken = TriggerCreate("libHAMA_gt_HeroAmazonSurgeofLightDamageTaken_Func");
    TriggerEnable(libHAMA_gt_HeroAmazonSurgeofLightDamageTaken, false);
    TriggerAddEventUnitDamaged(libHAMA_gt_HeroAmazonSurgeofLightDamageTaken, null, c_unitDamageTypeAny, c_unitDamageEither, null);
}

//--------------------------------------------------------------------------------------------------
// Trigger: Hero - Amazon - Surge of Light - Reset Charge
//--------------------------------------------------------------------------------------------------
bool libHAMA_gt_HeroAmazonSurgeofLightResetCharge_Func (bool testConds, bool runActions) {
    // Variable Declarations
    string lv_currentChargeReference;

    // Variable Initialization
    lv_currentChargeReference = "AmazonAvoidanceSurgeOfLightDummyCurrentCharge";

    // Actions
    if (!runActions) {
        return true;
    }

    CatalogFieldValueModify(c_gameCatalogEffect, lv_currentChargeReference, "Amount", EventPlayer(), "0", c_upgradeOperationSet);
    libNtve_gf_SetDialogItemCurrentValue(libHAMA_gv_amazonUI.lv_SurgeOfLightProgressBar, FixedToInt(0.0), libCore_gv_playerGroupFromPlayer[UnitGetOwner(EventUnit())]);
    return true;
}

//--------------------------------------------------------------------------------------------------
void libHAMA_gt_HeroAmazonSurgeofLightResetCharge_Init () {
    libHAMA_gt_HeroAmazonSurgeofLightResetCharge = TriggerCreate("libHAMA_gt_HeroAmazonSurgeofLightResetCharge_Func");
    TriggerEnable(libHAMA_gt_HeroAmazonSurgeofLightResetCharge, false);
    TriggerAddEventUnitBehaviorChange(libHAMA_gt_HeroAmazonSurgeofLightResetCharge, null, "AmazonAvoidanceSurgeOfLightAbilityReady", c_unitBehaviorChangeDestroy);
}

//--------------------------------------------------------------------------------------------------
// Trigger: Hero - Amazon - Inner Light (Check for CC)
//--------------------------------------------------------------------------------------------------
bool libHAMA_gt_HeroAmazonInnerLightCheckforCC_Func (bool testConds, bool runActions) {
    // Variable Declarations
    int lv_amazonPlayer;
    unit lv_amazonUnit;

    // Variable Initialization
    lv_amazonPlayer = EventPlayer();
    lv_amazonUnit = EventUnit();

    // Conditions
    if (testConds) {
        if (!((lv_amazonPlayer > 0))) {
            return false;
        }

        if (!((lv_amazonPlayer <= libCore_gv_bALMaxPlayers))) {
            return false;
        }

        if (!((PlayerHasTalent(lv_amazonPlayer, "AmazonInnerLight") == true))) {
            return false;
        }

        if (!((UnitGetType(lv_amazonUnit) == "HeroAmazon"))) {
            return false;
        }

        if (!((UnitHasBehavior2(lv_amazonUnit, "AmazonBlindingLightInnerLightCooldown") == false))) {
            return false;
        }

        if (!((UnitHasBehaviorWithFlag(lv_amazonUnit, c_unitBehaviorFlagUser1) == false))) {
            return false;
        }
    }

    // Actions
    if (!runActions) {
        return true;
    }

    PlayerCreateEffectUnit(EventPlayer(), "AmazonBlindingLightInnerLightApplySet", lv_amazonUnit);
    return true;
}

//--------------------------------------------------------------------------------------------------
void libHAMA_gt_HeroAmazonInnerLightCheckforCC_Init () {
    libHAMA_gt_HeroAmazonInnerLightCheckforCC = TriggerCreate("libHAMA_gt_HeroAmazonInnerLightCheckforCC_Func");
    TriggerEnable(libHAMA_gt_HeroAmazonInnerLightCheckforCC, false);
    TriggerAddEventUnitBehaviorChangeFromCategory(libHAMA_gt_HeroAmazonInnerLightCheckforCC, null, c_behaviorCategoryStun, c_unitBehaviorChangeCreate);
    TriggerAddEventUnitBehaviorChangeFromCategory(libHAMA_gt_HeroAmazonInnerLightCheckforCC, null, c_behaviorCategoryUser5, c_unitBehaviorChangeCreate);
}

void libHAMA_InitTriggers () {
    libHAMA_gt_AmazonHeroSpecificCommandCardPanelInitialize_Init();
    libHAMA_gt_AmazonUIInitialize_Init();
    libHAMA_gt_HeroAIMiscModInitialization_Init();
    libHAMA_gt_HeroAIAddToTierDataAmazon_Init();
    libHAMA_gt_HeroAIInitAbilitiesAmazon_Init();
    libHAMA_gt_HeroAmazonValkyrieCheckForStuckHero_Init();
    libHAMA_gt_HeroAmazonSurgeofLightDamageTaken_Init();
    libHAMA_gt_HeroAmazonSurgeofLightResetCharge_Init();
    libHAMA_gt_HeroAmazonInnerLightCheckforCC_Init();
}

//--------------------------------------------------------------------------------------------------
// Library Initialization
//--------------------------------------------------------------------------------------------------
bool libHAMA_InitLib_completed = false;

void libHAMA_InitLib () {
    if (libHAMA_InitLib_completed) {
        return;
    }

    libHAMA_InitLib_completed = true;

    libHAMA_InitLibraries();
    libHAMA_InitVariables();
    libHAMA_InitCustomScript();
    libHAMA_InitTriggers();
}

