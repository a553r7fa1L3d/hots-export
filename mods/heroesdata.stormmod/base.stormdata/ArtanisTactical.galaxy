//---------------------------------------------------------------------------------------------
// Artanis Tactical
//---------------------------------------------------------------------------------------------
const string c_AB_ArtanisPhasePrism         = "ArtanisPhasePrism"; // This is definitely in his abil.xml so
const string c_AB_ArtanisTwinBlades              = "ArtanisTwinBlades";
const string c_AB_ArtanisBladeDash             = "ArtanisBladeDash";
const string c_AB_ArtanisShieldOverloadDummy      = "ArtanisShieldOverloadDummy";
const string c_AB_ArtanisSpearOfAdunPurifierBeam  = "ArtanisSpearofAdunPurifierBeam";
const string c_AB_ArtanisSpearOfAdunSuppressionPulse = "ArtanisSpearofAdunSuppressionPulse";


const string c_ShieldOverloadAbility = "ArtanisShieldOverloadDummy";

//---------------------------------------------------------------------------------------------
fixed Storm_AI_PreferNonWarriorTargetsNotInDanger (unitgroup scanGroup, int player, unit aiUnit, unit targetUnit, fixed targetScore, point targetPosition) {
    fixed score;
    int targetPlayer;

    // If target is in an allied AOE, do not reposition.
    score = Storm_AI_IgnoreTargetsInAOEDangerousToThem(scanGroup, player, aiUnit, targetUnit, targetScore, targetPosition);

    // reduce the score of warrior targets
    if (score != c_Storm_AI_InvalidScore) {
        targetPlayer = UnitGetOwner(targetUnit);
        if (targetPlayer > 0 && targetPlayer < libCore_gv_bALMaxPlayers && libGame_gv_players[targetPlayer].lv_heroData.lv_class == c_heroRoleWarrior) {
            return targetScore - c_Storm_AI_FullHeroScore * 0.5;
        }
    }
    return score;
}

//---------------------------------------------------------------------------------------------
bool ArtanisPhasePrism(int player, unit aiUnit, unitgroup scanGroup) {
    int enemiesInArea;
    int alliesInArea;
    int allyIndex;
    int allyPlayer;
    unit targetHero;
    unitgroup allyGroup;
    order spellOrd;
    order attackOrd;
    fixed currentHealthShield;
    fixed totalHealth;
    Storm_AI_TargetQueryOptions query;

    // Prefer auto attacking over ability use, especially as the game goes on.
    if (libCore_gf_IsAbilityOnCooldown(aiUnit, c_ShieldOverloadAbility)) {
        return false;
    }

    spellOrd = StormHeroAICreateOrder(player, c_AB_ArtanisPhasePrism, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }

    attackOrd = StormHeroAICreateOrder(player, c_Storm_AB_Attack, 0);
    if (!UnitOrderIsValid(aiUnit, attackOrd)) {
        return false;
    }

    // If there are no enemies, we don't do this at all.
    enemiesInArea = CountEnemiesInArea(scanGroup, UnitGetPosition(aiUnit), 8.0, 1, 0);
    if (enemiesInArea < 1) {
        return false;
    }

    // Don't jump into battle with low health
    currentHealthShield = UnitGetPropertyFixed(aiUnit, c_unitPropLife, c_unitPropCurrent) + UnitGetPropertyFixed(aiUnit, c_unitPropShields, c_unitPropCurrent);
    totalHealth = UnitGetPropertyFixed(aiUnit, c_unitPropLifeMax, c_unitPropCurrent);
    if (((currentHealthShield / totalHealth) * 100.0) < libAIAI_gv_aIHeroes[player].lv_startOffensiveHealth) {
        return false;
    }

    query.lv_maxDistance = 7.0;
    query.lv_minDistance = 3.0;
    query.lv_maxHealthPercent = libAIAI_gv_aIHeroMediumHealthPercent;
    query.lv_healthFactor = c_Storm_AI_PreferUnhealthyTargets;
    query.lv_distanceFactor = c_Storm_AI_PreferCloserTargets;
    query.lv_ignoreTargetsBehindGate = true;
    query.lv_ignoreTargetsBehindAlliedGate = true;
    query.lv_adjustScoreCallback = Storm_AI_PreferNonWarriorTargetsNotInDanger;
    targetHero = FindBestHero(scanGroup, aiUnit, query);

    if (!UnitIsValid(targetHero)) {
        return false;
    }

    // If they are super low health, go to town now pain train no brakes.
    if (UnitGetPropertyFixed(targetHero, c_unitPropLifePercent, c_unitPropCurrent) < 10.0) {
        libAIAI_gf_HeroAIStartFocusHero(player, targetHero);
        return HeroIssueOrder(player, aiUnit, spellOrd, attackOrd, c_orderQueueAddToFront, targetHero, null);
    }

    // If I'm in an enemy AOE or in an allied AOE, try to swap with an enemy
    if (libAIAI_gf_HeroAIIsPointInAOEDangerousToFaction(UnitGetPosition(aiUnit), libAIAI_gv_aIHeroAvoidAOEAvoidExtraDistance, libGame_gv_players[player].lv_faction)
        || libAIAI_gf_HeroAIIsPointInAOEDangerousToFaction(UnitGetPosition(aiUnit), libAIAI_gv_aIHeroAvoidAOEAvoidExtraDistance, GetOpposingFaction(libGame_gv_players[player].lv_faction))) {
        return HeroIssueOrder(player, aiUnit, spellOrd, attackOrd, c_orderQueueAddToFront, targetHero, null);
    }
    
    alliesInArea = UnitGroupCount(libAIAI_gv_aIHeroes[player].lv_allyHeroes, c_unitCountAll);
    if (alliesInArea < enemiesInArea) {
        return false;
    }

    libAIAI_gf_HeroAIStartFocusHero(player, targetHero);
    return HeroIssueOrder(player, aiUnit, spellOrd, attackOrd, c_orderQueueAddToFront, targetHero, null);
}

//---------------------------------------------------------------------------------------------
bool ArtanisTwinBlades(int player, unit aiUnit, unitgroup scanGroup) {
    // Very low cost, essentially an always use ability.
    int enemiesInArea = 0;
    order spellOrd;
    unit targetEnemy;
    Storm_AI_TargetQueryOptions query;

    
    spellOrd = StormHeroAICreateOrder(player, c_AB_ArtanisTwinBlades, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }
    
    query.lv_maxDistance = 5.0;
    query.lv_minDistance = 0.0;
    query.lv_maxHealthPercent = 100.0;
    query.lv_healthFactor = c_Storm_AI_PreferUnhealthyTargets;
    query.lv_distanceFactor = c_Storm_AI_StronglyPreferCloserTargets;
    query.lv_ignoreTargetsBehindGate = true;

    targetEnemy = FindBestHero(scanGroup, aiUnit, query);
    if (!UnitIsValid(targetEnemy)) {
        targetEnemy = FindBestMinion(scanGroup, aiUnit, query);
        if (!UnitIsValid(targetEnemy)) {
            targetEnemy = FindBestAggroCreep(scanGroup, aiUnit, query);
            if (!UnitIsValid(targetEnemy)) {
                // There are no enemies nearby, don't cast.
                return false; 
            }
        }
    }

    return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, null);
}

//---------------------------------------------------------------------------------------------
bool ArtanisBladeDash(int player, unit aiUnit, unitgroup scanGroup) {
    
    unit targetEnemy; 
    order spellOrd;
    Storm_AI_TargetQueryOptions query;
    fixed currentHealthShield;
    fixed totalHealth;
    bool shieldOnCD;

    shieldOnCD = libCore_gf_IsAbilityOnCooldown(aiUnit, c_ShieldOverloadAbility);

    spellOrd = StormHeroAICreateOrder(player, c_AB_ArtanisBladeDash, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }

    query.lv_maxDistance = 7.0;
    query.lv_minDistance = 0.0;
    query.lv_maxHealthPercent = 5.0;
    query.lv_healthFactor = c_Storm_AI_PreferUnhealthyTargets;
    query.lv_distanceFactor = c_Storm_AI_PreferDistantTargets;
    query.lv_ignoreTargetsBehindGate = true;
    targetEnemy = FindBestHero(scanGroup, aiUnit, query);

    // If the target enemy is incredibly low health, we go in for the kill, regardless.
    // Try heroes first.
    if (UnitIsValid(targetEnemy)) {
        return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, targetEnemy, UnitGetPosition(targetEnemy));
    }

    // Prefer auto attacking over ability use, especially as the game goes on.
    // Don't jump into battle with low health
    currentHealthShield = UnitGetPropertyFixed(aiUnit, c_unitPropLife, c_unitPropCurrent) + UnitGetPropertyFixed(aiUnit, c_unitPropShields, c_unitPropCurrent);
    totalHealth = UnitGetPropertyFixed(aiUnit, c_unitPropLifeMax, c_unitPropCurrent);
    if (shieldOnCD && ((currentHealthShield / totalHealth) * 100.0) < libAIAI_gv_aIHeroes[player].lv_startOffensiveHealth) {
        return false;
    }

    // Try minions and creeps if I have decent mana.
    if (!shieldOnCD && UnitGetPropertyFixed(aiUnit, c_unitPropEnergyPercent, c_unitPropCurrent) >= libAIAI_gv_aIHeroLowEnergyPercent) {
        // reset the health calculation so I can get fresh minions and heroes
        query.lv_maxHealthPercent = 100.0;
        query.lv_adjustScoreCallback = Storm_AI_IgnoreTargetsInAOEDangerousToUs;
        targetEnemy = FindBestTacticalTarget(scanGroup, aiUnit, query);
        if (targetEnemy != null && EnoughEnemiesInArea(scanGroup, UnitGetPosition(targetEnemy), 2.0, c_Storm_AI_AtLeast2Heroes, c_Storm_AI_AtLeast2Creeps, c_Storm_AI_AtLeast4Minions)) {
            return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, targetEnemy, UnitGetPosition(targetEnemy));
        }
    }

    return false;
}

//---------------------------------------------------------------------------------------------
bool ArtanisSpearofAdunPurifierBeam(int player, unit aiUnit, unitgroup scanGroup) {
    order ord;
    int numVisibleEnemies;
    int alliesInArea;
    int numEnemyHeroes;
    int enemyHeroIndex;
    unit enemyHero;
    unit lowestEnemy;
    point enemyPos;
    region regionCircle;

    ord = StormHeroAICreateOrder(player, c_AB_ArtanisSpearOfAdunPurifierBeam, 0);
    if (!UnitOrderIsValid(aiUnit, ord)) {
        return false;
    }

    numVisibleEnemies = UnitGroupCount(libAIAI_gv_aIHeroes[player].lv_visibleEnemyHeroes, c_unitCountAll);
    for (enemyHeroIndex = 1; enemyHeroIndex <= numVisibleEnemies; enemyHeroIndex += 1) {

        enemyHero = UnitGroupUnit(libAIAI_gv_aIHeroes[player].lv_visibleEnemyHeroes, enemyHeroIndex);

        if (UnitGetPropertyFixed(enemyHero, c_unitPropLifePercent, c_unitPropCurrent) <= libAIAI_gv_aIHeroLowHealthPercent) {
            return HeroIssueOrder(player, aiUnit, ord, null, c_orderQueueReplace, enemyHero, null);
        }
    }
    return false;
}

//---------------------------------------------------------------------------------------------
bool ArtanisSpearofAdunSuppressionPulse(int player, unit aiUnit, unitgroup scanGroup) {

    order ord;
    int numVisibleEnemies;
    int enemyHeroIndex;
    unit enemyHero;
    point enemyPos;
    fixed webRadius = 8.0;
    region regionCircle;
    
    ord = StormHeroAICreateOrder(player, c_AB_ArtanisSpearOfAdunSuppressionPulse, 0);
    if (!UnitOrderIsValid(aiUnit, ord)) {
        return false;
    }

    // Prefer to check the scangroup first, help me immediately in a teamfight, then go to visible.
    if (RequiredEnemyPlayerCountInArea(3, scanGroup, UnitGetPosition(aiUnit), webRadius) && RequiredAllyPlayerCountInArea(2, player, UnitGetPosition(aiUnit), 10.0)) {
        // Let's blast em if there are this many near me.
        return HeroIssueOrder(player, aiUnit, ord, null, c_orderQueueReplace, null, UnitGetPosition(aiUnit));
    }

    // If we get here, they weren't close to me, so let's check visible heroes and see if I can affect a teamfight.
    numVisibleEnemies = UnitGroupCount(libAIAI_gv_aIHeroes[player].lv_visibleEnemyHeroes, c_unitCountAll);
    for (enemyHeroIndex = 1; enemyHeroIndex <= numVisibleEnemies; enemyHeroIndex += 1) {
        // Center the region circle on the current unit, if within that circle there are 3 enemies, we cast.
        enemyHero = UnitGroupUnit(libAIAI_gv_aIHeroes[player].lv_visibleEnemyHeroes, enemyHeroIndex);
        enemyPos = UnitGetPosition(enemyHero);
        regionCircle = RegionCircle(enemyPos, webRadius);
        // We count both allies and enemies because this is a teamfight ability.
        if (RequiredEnemyPlayerCountInArea(3, scanGroup, enemyPos, webRadius) && RequiredAllyPlayerCountInArea(2, player, enemyPos, webRadius)) {
            return HeroIssueOrder(player, aiUnit, ord, null, c_orderQueueReplace, null, enemyPos);
        }
    }
    return false;
}

void AIThinkArtanis(int player, unit aiUnit, unitgroup scanGroup) {

    if (HeroSkipTactical(player, aiUnit)) {
        return;
    }

    if (UseDefensiveItem(player, aiUnit, scanGroup)) {
        return;
    }

    if (HeroSkipOffensiveTactical(player, aiUnit)) {
        return;
    }
    // Don't early out, he should use these AND another ability.
    ArtanisTwinBlades(player, aiUnit, scanGroup);

    if (ArtanisBladeDash(player, aiUnit, scanGroup)) {
        return;
    }

    if (ArtanisPhasePrism(player, aiUnit, scanGroup)) {
        return;
    }

    if (libAIAI_gf_HeroAIShouldUseUltimates(player) && ArtanisSpearofAdunPurifierBeam(player, aiUnit, scanGroup)) {
        return;
    }

    if (libAIAI_gf_HeroAIShouldUseUltimates(player) && ArtanisSpearofAdunSuppressionPulse(player, aiUnit, scanGroup)) {
        return;
    }

    if (UseOffensiveItem(player, aiUnit, scanGroup)) {
        return;
    }

    if (RevealCloakedEnemies(player, aiUnit, scanGroup)) {
        return;
    }
}