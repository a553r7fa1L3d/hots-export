//---------------------------------------------------------------------------------------------
// Dryad Tactical
//---------------------------------------------------------------------------------------------
const string c_AB_DryadNoxiousBlossom          = "DryadNoxiousBlossom";
const string c_AB_DryadCripplingSpores            = "DryadCripplingSpores";
const string c_AB_DryadWisp                 = "DryadWisp";
const string c_AB_DryadWispRedirect         = "DryadWispRedirect";
const string c_AB_DryadLeapingStrike      = "DryadLeapingStrike";
const string c_AB_DryadThornwoodVine            = "DryadThornwoodVine";
const string c_AB_DryadGallopingGait        = "DryadGallopingGait";
const fixed c_DryadNoxiousBlossomRange         = 7.0;
const fixed c_DryadNoxiousBlossomRadius        = 2.0;
const fixed c_DryadLeapingStrikeRange     = 5.5;

const fixed c_DryadPathRange                = 30.0;

point[libCore_gv_bALMaxPlayers + 1] WispEndPoints;
unit[libCore_gv_bALMaxPlayers + 1]  LunaraWispCache;

//---------------------------------------------------------------------------------------------
// Helper for determining where to cast a wisp
bool DryadCastWisp (int player, unit aiUnit, unit wispUnit, order spellOrd) {
    int campIndex;
    int loopIndex = 1;
    int validCampCount = 0;
    int targetCampIndex;
    int randomCampIndex;
    int[libMapM_gv_jungleMaxCamps] validCampIndices;
    point targetPosition;
    bool orderFaction = false;
    fixed offsetRadius = 2.0;

    if (libGame_gv_players[player].lv_faction == libGame_ge_Faction_Order) {
        orderFaction = true;
    }
    
    // Don't cast if camps have not spawned yet
    if (orderFaction) {   
        if (libAIAI_gv_heroAITeamCampData[0].lv_startCampCheckTimer < TimerGetElapsed(libGame_gv_gameTimer)) {
            return false;
        }
    }
    else {
        if (libAIAI_gv_heroAITeamCampData[1].lv_startCampCheckTimer < TimerGetElapsed(libGame_gv_gameTimer)) {
            return false;
        }
    }

    // Try placing Wisp at one of our camps
    for (loopIndex = 1; loopIndex <= libMapM_gv_jungleMaxCamps; loopIndex += 1) {
        if (orderFaction) {
            campIndex = libAIAI_gv_heroAIOrderMercCampList[loopIndex];
        }
        else {
            campIndex = libAIAI_gv_heroAIChaosMercCampList[loopIndex];
        }

        if (campIndex <= 0) {
            break;
        }

        // We only want to scout a camp that we know has not been taken
        if (UnitGetOwner(libMapM_gv_jungleCreepCamps[campIndex].lv_campOwnershipFlagUnit) == libCore_gv_cOMPUTER_Hostile) {
            // Try another camp if this one is already visible
            if (VisIsVisibleForPlayer(player, libMapM_gv_jungleCreepCamps[campIndex].lv_mapDataCampCaptainSpawnPoint)) {
                continue;
            }

            // Add the camp index to our possible candidates
            if (validCampCount + 1 < libMapM_gv_jungleMaxCamps) {
                validCampIndices[validCampCount] = campIndex;
                validCampCount += 1;
            }
            else {
                break;
            }
        }
    }
    
    // Try placing Wisp at a middle camp
    for (loopIndex = 1; loopIndex <= libMapM_gv_jungleMaxCamps; loopIndex += 1) {
        campIndex = libAIAI_gv_heroAIMiddleMercCampList[loopIndex];
        if (campIndex <= 0) {
            break;
        }
        
        // We only want to scout a camp that we know has not been taken
        if (UnitGetOwner(libMapM_gv_jungleCreepCamps[campIndex].lv_campOwnershipFlagUnit) == libCore_gv_cOMPUTER_Hostile) {
            // Try another camp if this one is already visible
            if (VisIsVisibleForPlayer(player, libMapM_gv_jungleCreepCamps[campIndex].lv_mapDataCampCaptainSpawnPoint)) {
                continue;
            }

            // Add the camp index to our possible candidates
            if (validCampCount + 1 < libMapM_gv_jungleMaxCamps) {
                validCampIndices[validCampCount] = campIndex;
                validCampCount += 1;
            }
            else {
                break;
            }
        }
    }


    // Try placing Wisp at an enemy camp
    for (loopIndex = 1; loopIndex <= libMapM_gv_jungleMaxCamps; loopIndex += 1) {
        if (orderFaction) {
            campIndex = libAIAI_gv_heroAIChaosMercCampList[loopIndex];
        }
        else {
            campIndex = libAIAI_gv_heroAIOrderMercCampList[loopIndex];
        }

        if (campIndex <= 0) {
            break;
        }

        // We only want to scout a camp that we know has not been taken
        if (UnitGetOwner(libMapM_gv_jungleCreepCamps[campIndex].lv_campOwnershipFlagUnit) == libCore_gv_cOMPUTER_Hostile) {
            // Try another camp if this one is already visible
            if (VisIsVisibleForPlayer(player, libMapM_gv_jungleCreepCamps[campIndex].lv_mapDataCampCaptainSpawnPoint)) {
                continue;
            }
            
            // Add the camp index to our possible candidates
            if (validCampCount + 1 < libMapM_gv_jungleMaxCamps) {
                validCampIndices[validCampCount] = campIndex;
                validCampCount += 1;
            }
            else {
                break;
            }
        }
    }
    
    // If we found no valid camps, don't cast a wisp
    if (validCampCount == 0) {
        return false;
    }

    // Pick a random camp index from our valid camps to use as our target camp index
    randomCampIndex = RandomInt(0, validCampCount - 1);
    targetCampIndex = validCampIndices[randomCampIndex];

    // Randomize the point slightly to direct our Wisp to
    WispEndPoints[player] = PointWithOffsetPolar(libMapM_gv_jungleCreepCamps[targetCampIndex].lv_mapDataCampCaptainSpawnPoint, RandomFixed(0.0, 1.5), libNtve_gf_RandomAngle());
    if (wispUnit != null) {
        targetPosition = AIPositionAlongPath(player, UnitGetPosition(wispUnit), WispEndPoints[player], c_DryadPathRange, true, c_includeAllBuildings);
    } 
    else {
        targetPosition = WispEndPoints[player]; 
    }
    return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, targetPosition);
}

//---------------------------------------------------------------------------------------------
// Helper for selecting the correct Wisp unit
unit DryadGetWisp (int player) {
    unitgroup wispGroup = UnitGroup("DryadWispUnit", player, RegionEntireMap(), null, 1);
    int wispCount = UnitGroupCount(wispGroup, c_unitCountAll);
    
    if (wispCount > 0) {
        return UnitGroupUnit(wispGroup, 1);
    }

    return null;
}

//---------------------------------------------------------------------------------------------
bool DryadNoxiousBlossom (int player, unit aiUnit, unitgroup scanGroup) {
    // After .5 seconds, the target area of 1.5 radius explodes and deals damage to units within it
    order spellOrd;
    unit targetEnemy;
    point targetPos;
    fixed energy = UnitGetPropertyFixed(aiUnit, c_unitPropEnergyPercent, c_unitPropCurrent);
    Storm_AI_TargetQueryOptions query;

    spellOrd = StormHeroAICreateOrder(player, c_AB_DryadNoxiousBlossom, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }

    query.lv_maxDistance = c_DryadNoxiousBlossomRange;
    query.lv_healthFactor = c_Storm_AI_PreferUnhealthyTargets;

    if (energy > libAIAI_gv_aIHeroMediumEnergyPercent) {
        targetEnemy = FindBestTacticalTarget(scanGroup, aiUnit, query);
    }
    else {
        // wounded heroes only
        query.lv_maxHealthPercent = libAIAI_gv_aIHeroMediumHealthPercent;
        targetEnemy = FindBestHero(scanGroup, aiUnit, query);
    }

    if (targetEnemy == null) {
        if (energy > libAIAI_gv_aIHeroHighEnergyPercent) {
            // cast on a tower
            targetEnemy = FindBestUnit(UnitGroupFilterTowers(scanGroup), scanGroup, aiUnit, query);
            if (targetEnemy != null) {
                return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, UnitGetPosition(targetEnemy));
            }
        }
        return false;
    }
    
    targetPos = UnitGetPosition(targetEnemy);
    if (!EnoughEnemiesInArea(scanGroup, targetPos, c_DryadNoxiousBlossomRadius, c_Storm_AI_AtLeast1Hero, c_Storm_AI_AtLeast2Creeps, c_Storm_AI_AtLeast2Minions)) {
        return false;
    }    

    return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, targetPos);
}

//---------------------------------------------------------------------------------------------
bool DryadCripplingSpores (int player, unit aiUnit, unitgroup scanGroup) {
    // Increases duration and slows enemies who have the NaturesToxin debuff
    // We actually want to keep this for saving ourselves or during a teamfight.
    order spellOrd;
    fixed energy;
    unitgroup testGroup;
    unit currentUnit;
    int enemyHeroesDebuffed;
    int enemiesDebuffed;
    int unitCount;
    int index;

    spellOrd = StormHeroAICreateOrder(player, c_AB_DryadCripplingSpores, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }
    // This is the case that we are being attacked and must run.
    if (UnitGetPropertyFixed(aiUnit, c_unitPropLifePercent, c_unitPropCurrent) < libAIAI_gv_aIHeroes[player].lv_endOffensiveHealth
        && HaveBeenAttackedRecently(aiUnit)) {
        return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, null);
    }

    energy = UnitGetPropertyFixed(aiUnit, c_unitPropEnergyPercent, c_unitPropCurrent);
    enemyHeroesDebuffed = 0;
    enemiesDebuffed = 0;
    unitCount = UnitGroupCount(scanGroup, c_unitCountAll);
    index = 0;
    
    // Check for units in the scangroup that have the NaturesToxin behavior
    for (index = 1; index <= 1; index += 1) {
        currentUnit = UnitGroupUnit(scanGroup, index);
        if (UnitHasBehavior2(currentUnit, "DryadNaturesToxin")) {
            if (UnitTypeTestAttribute(UnitGetType(currentUnit), c_unitAttributeHeroic)) {
                enemyHeroesDebuffed += 1;
            }
            else {
                enemiesDebuffed += 1;
            }
        }
    }
    
    // Cast if at least one enemy hero has the debuff, regardless of energy percentage
    if (enemyHeroesDebuffed > 0) {
        return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, null);
    }
    
    // Only cast if at least 3 non-heroes have the debuff
    if (energy > libAIAI_gv_aIHeroLowEnergyPercent && enemiesDebuffed > 2) {
        return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, null);
    }
    
    return false;
}

//---------------------------------------------------------------------------------------------
bool DryadWisp (int player, unit aiUnit, unitgroup scanGroup) {
    // Summons a wisp to give vision in a target area, unlimited range
    order spellOrd;

    spellOrd = StormHeroAICreateOrder(player, c_AB_DryadWisp, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }

    return DryadCastWisp(player, aiUnit, null, spellOrd);
}

//---------------------------------------------------------------------------------------------
bool DryadWispRedirect (int player, unit aiUnit, unitgroup scanGroup) {
    // Redirect the wisp to a new location
    order spellOrd;
    unit wispUnit;
    point wispPos;
    point targetPos;

    spellOrd = StormHeroAICreateOrder(player, c_AB_DryadWispRedirect, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }

    if (UnitIsAlive(LunaraWispCache[player])) {
        wispUnit = LunaraWispCache[player];
    }
    else {
        // we have a live wisp, but we havent recorded it yet
        // This could potentially be null, but that is still ok with us
        wispUnit = DryadGetWisp(player);
        LunaraWispCache[player] = wispUnit;
    }

    if (!UnitIsValid(wispUnit)) {
        return false;
    }

    // If the current order is not no order, then the wisp is already moving, so we should
    // not cast a redirect. This is to ensure that the wisp does not move too often.
    if (UnitOrder(wispUnit, 0) != null) {
        return false;
    }

    wispPos = UnitGetPosition(wispUnit);
    if (RegionContainsPoint(RegionCircle(WispEndPoints[player], 8.0), wispPos)) {
        //we are within range of the point, we need to find a new point.
        return DryadCastWisp(player, aiUnit, wispUnit, spellOrd);
    }
   // Else we need to get a new path to the point.
    targetPos = AIPositionAlongPath(player, wispPos, WispEndPoints[player], c_DryadPathRange, true, c_includeAllBuildings);
    // Else we need to move randomly because we can't get there.
    if (targetPos == null) {
        targetPos = PointWithOffsetPolar(wispPos, RandomFixed(8.0, 16.0), libNtve_gf_RandomAngle());
    }
    return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, targetPos);
}

//---------------------------------------------------------------------------------------------
bool DryadLeapingStrike (int player, unit aiUnit, unitgroup scanGroup) {
    // Leap over the target enemy and deal damage to them
    unitgroup landingScanGroup;
    int charges;
    unit targetUnit;
    order spellOrd;
    point landingPosition;
    fixed health = UnitGetPropertyFixed(aiUnit, c_unitPropLifePercent, c_unitPropCurrent);
    fixed energy = UnitGetPropertyFixed(aiUnit, c_unitPropEnergyPercent, c_unitPropCurrent);
    Storm_AI_TargetQueryOptions query;

    spellOrd = StormHeroAICreateOrder(player, c_AB_DryadLeapingStrike, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }

    // Don't cast if our health is way too low
    if (health < libAIAI_gv_aIHeroes[player].lv_returnToSpawnHealth) {
        return false;
    }

    // Cast if enemy health is low enough
    if (energy > libAIAI_gv_aIHeroHighEnergyPercent) {
        query.lv_maxHealthPercent = libAIAI_gv_aIHeroHighHealthPercent;
    }
    else {
        query.lv_maxHealthPercent = libAIAI_gv_aIHeroMediumHealthPercent;
    }

    // Find a target hero
    query.lv_maxDistance = c_DryadLeapingStrikeRange;
    query.lv_ignoreTargetsBehindGate = true;
    query.lv_healthFactor = c_Storm_AI_StronglyPreferUnhealthyTargets;
    targetUnit = FindBestHero(scanGroup, aiUnit, query);

    // Try to cast on this target enemy hero
    charges = FixedToInt(UnitAbilityChargeInfo(aiUnit, AbilityCommand("DryadLeapingStrike", 0), c_unitAbilChargeCountLeft));
    if (UnitIsValid(targetUnit)) {
        // If we have 2, then we can use it to jump back and forth
        if (charges == 2) {
            // Jump past the target hero dealing damage to it, then jump past them a second time
            // dealing damage again and returning us to our previous position
            HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, targetUnit, null);
            HeroClaimForTactical(player, 1.0, false);
            return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, targetUnit, null);
        }
    }

    // If we are too weak to jump into battle, try to cast on with the intention to use skill as an escape
    if (health < libAIAI_gv_aIHeroes[player].lv_startOffensiveHealth) {
        // Find a target minion or hero with the intention to use this skill as an escape
        query.lv_maxHealthPercent = 100.0;
        query.lv_distanceFactor = c_Storm_AI_PreferDistantTargets;
        if (HaveBeenAttackedRecently(aiUnit) && health <= libAIAI_gv_aIHeroes[player].lv_returnToSpawnHealth) {
            targetUnit = FindBestTacticalTarget(scanGroup, aiUnit, query);
        }
    }

    if (!UnitIsValid(targetUnit)) {
        return false;
    }
        
    landingPosition = libNtve_gf_PointOffsetTowardsPoint(UnitGetPosition(aiUnit), c_DryadLeapingStrikeRange, UnitGetPosition(targetUnit));
    landingScanGroup = UnitGroupAlliance(player, c_unitAllianceEnemy, RegionCircle(landingPosition, c_DryadLeapingStrikeRange), null,  c_noMaxCount);
   
    // Only want to jump to the target unit if our team will not be outnumbered at the landing position.
    if (CountTeamOutnumberedBy(player, landingScanGroup, landingPosition, c_DryadLeapingStrikeRange) <= 0) {
        return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, targetUnit, null);
    }

    return false;
}

//---------------------------------------------------------------------------------------------
bool DryadThornwoodVine (int player, unit aiUnit, unitgroup scanGroup) {
    // Hurl a spear, dealing damage to all enemies within a line
    unit targetHero;
    order spellOrd;
    order attackOrd;
    fixed energy = UnitGetPropertyFixed(aiUnit, c_unitPropEnergyPercent, c_unitPropCurrent);
    Storm_AI_TargetQueryOptions query;

    spellOrd = StormHeroAICreateOrder(player, c_AB_DryadThornwoodVine, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }

    if (energy > libAIAI_gv_aIHeroHighEnergyPercent) {
        query.lv_maxHealthPercent = libAIAI_gv_aIHeroHighHealthPercent;
    }
    else  {
        query.lv_maxHealthPercent = libAIAI_gv_aIHeroMediumHealthPercent;
    }

    query.lv_maxDistance = 10.0;
    query.lv_healthFactor = c_Storm_AI_PreferUnhealthyTargets;
    query.lv_distanceFactor = c_Storm_AI_PreferCloserTargets;

    // Find a target hero 
    targetHero = FindBestHero(scanGroup, aiUnit, query);
    if (!UnitIsValid(targetHero)) { 
        return false;
    }

    return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, UnitGetPosition(targetHero));
}

//---------------------------------------------------------------------------------------------
bool DryadGallopingGait (int player, unit aiUnit, unitgroup scanGroup) {
    // Increases movement speed for 6 seconds
    // Similar to SgtHammer's Thrusters, for now only use to flee.
    order spellOrd;

    spellOrd = StormHeroAICreateOrder(player, c_AB_DryadGallopingGait, 0);
    if (!UnitOrderIsValid(aiUnit, spellOrd)) {
        return false;
    }

    // Retreat if you have low health and have been attacked recently
    if (HaveBeenAttackedRecently(aiUnit) && UnitGetPropertyFixed(aiUnit, c_unitPropLifePercent, c_unitPropCurrent) <= libAIAI_gv_aIHeroes[player].lv_returnToSpawnHealth) {
        return HeroIssueOrder(player, aiUnit, spellOrd, null, c_orderQueueAddToFront, null, null);
    }

    return false;
}

//---------------------------------------------------------------------------------------------
bool DryadDefensiveItemCallback (int player, unit aiUnit, unitgroup scanGroup, string itemType, order ord, order ordTarget, order ordTogOn, order ordTogOff) {
    unit target;    
    fixed health = UnitGetPropertyFixed(aiUnit, c_unitPropLifePercent, c_unitPropCurrent);

    if (itemType == "DryadAbolishMagicItem") {
        target = GetAllyToCleanse(player, aiUnit);
        if (!UnitIsValid(target)) {
            health = UnitGetPropertyFixed(aiUnit, c_unitPropLifePercent, c_unitPropCurrent);
            if ((health <= libAIAI_gv_aIHeroes[player].lv_returnToSpawnHealth) && UnitHasDebuffThatCanBeCleansed(aiUnit)) {
                // willing to use on just ourself if low health
                target = aiUnit;
            }
            else {
                return false;
            }
        }

        return HeroIssueOrder(player, aiUnit, ordTarget, null, c_orderQueueAddToFront, target, null);
    }
    return false;
}

//---------------------------------------------------------------------------------------------
void AIThinkDryad (int player, unit aiUnit, unitgroup scanGroup) {
    
    if (HeroSkipTactical(player, aiUnit)) {
        return;
    }

    if (UseItem(player, aiUnit, scanGroup, DryadDefensiveItemCallback)) {
        return;
    }
    
    if (DryadGallopingGait(player, aiUnit, scanGroup)) {
        return;
    }

    if (DryadWisp(player, aiUnit, scanGroup)) {
        return;
    }

    if (HeroSkipOffensiveTactical(player, aiUnit)) {
        return;
    }

    // No offensive items used currently
    //if (UseOffensiveItem(player, aiUnit, scanGroup)) {
    //    return;
    //} 

    if (libAIAI_gf_HeroAIShouldUseUltimates(player) && DryadLeapingStrike(player, aiUnit, scanGroup)) {
        return;
    }

    if (libAIAI_gf_HeroAIShouldUseUltimates(player) && DryadThornwoodVine(player, aiUnit, scanGroup)) {
        return;
    }

    if (DryadNoxiousBlossom(player, aiUnit, scanGroup)) {
        return;
    }

    if (DryadCripplingSpores(player, aiUnit, scanGroup)) {
        return;
    }
    // We don't want to return early if we redirect
    DryadWispRedirect(player, aiUnit, scanGroup);

    if (RevealCloakedEnemies(player, aiUnit, scanGroup)) {
        return;
    }
}