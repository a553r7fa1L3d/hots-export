<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Desc>
    <!-- This is the parent frame for the Empty/Filled Cannonball Images -->  
    <Frame type="Frame" name="ThrallTraitCounterFrame">
        <Width val="84"/>
        <Height val="80"/>
        
        <Frame type="Image" name="ChargedTrait">
            <Anchor side="Top" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
            <Texture val=""/>
            <Visible val="true"/>
            <Animation name="FadePing">
                <Event event="OnShown" action="Play"/>
                <Event event="OnShown" action="DirectionForward"/>
                <Event event="OnHide" action="Play"/>
                <Event event="OnHide" action="DirectionReverse"/>
                <Controller type="Fade" end="PingPong">
                    <Key type="Curve" time="0" value="190" out="Slow"/>
                    <Key type="Curve" time=".4" value="255" in="Slow"/>
                </Controller>
            </Animation>
        </Frame>
    </Frame>
    
    
    <!-- This is the main Frame (Parent Frame) -->  
    <Frame type="Frame" name="ThrallTraitFrame">
        <Height val="200"/>
        <Width val="500"/>
        <Anchor side="Bottom" relative="$parent" pos="Max" offset="0"/>
        <Anchor side="Left" relative="$parent" pos="Min" offset="4"/>

        <!-- This is to get a green flash when the Trait procs -->  
        <Frame type="Image" name="BorderImage">
            <Anchor relative="$parent"/>
            
            <!-- Hide this frame initially.  Animations will cause it to show. -->
            <Visible val="false"/>
            
            <!-- Fade in when the hero frame transitions to the top of the stack. -->
            <Animation name="TransitionToTop">
                <Event event="TransitionToTop" action="Restart" frame="$parent"/>
                <Event event="TransitionToTop" action="Play" frame="$parent"/>
            </Animation>
        </Frame>
        
        <!-- This is the Trait Button Icon) -->
        <Frame type="Image" name="ThrallTraitIcon">
            <AcceptsMouse val="True"/>
            <Width val="116"/>
            <Height val="114"/>
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="-4"/>
            <Anchor side="Left" relative="$parent" pos="Min" offset="15"/>
            <Texture val="@UI/Storm_UI_HUD_HeroMechanic_Thrall_Mainimage"/>

            <Frame type="BehaviorIcon" name="BehaviorTooltipFrame">
                <Anchor relative="$parent"/>
                <TooltipFrame val="StandardNameTooltip"/>
                <Behavior val="ThrallFrostwolfResilienceStack"/>
            </Frame>
        </Frame>
        
        <!-- These are the Charge semi-circles-->   
        <Frame type="Frame" name="ThrallChargeFrame5" template="Thrall/ThrallTraitCounterFrame">
            <Anchor side="Top" relative="$parent/ThrallTraitIcon" pos="Min" offset="-4"/>
            <Anchor side="Right" relative="$parent/ThrallTraitIcon" pos="Max" offset="1"/>
            <Width val="64"/>
            <Height val="72"/>
            <Visible val="true"/>
            <Frame type="Image" name="ChargedTrait">
                <Anchor relative="$parent"/>
                <Texture val="@UI/Storm_UI_HUD_HeroMechanic_Thrall_Charge5"/>
            </Frame>
        </Frame>
        
        <Frame type="Frame" name="ThrallChargeFrame4" template="Thrall/ThrallTraitCounterFrame">
            <Anchor side="Bottom" relative="$parent/ThrallTraitIcon" pos="Max" offset="20"/>
            <Anchor side="Right" relative="$parent/ThrallTraitIcon" pos="Max" offset="21"/>
            <Width val="96"/>
            <Height val="104"/>
            <Frame type="Image" name="ChargedTrait">
                <Anchor relative="$parent"/>
                <Texture val="@UI/Storm_UI_HUD_HeroMechanic_Thrall_Charge4"/>
            </Frame>
        </Frame>
        <Frame type="Frame" name="ThrallChargeFrame3" template="Thrall/ThrallTraitCounterFrame">
            <Anchor side="Bottom" relative="$parent/ThrallTraitIcon" pos="Max" offset="20"/>
            <Anchor side="Left" relative="$parent/ThrallTraitIcon" pos="Min" offset="-16"/>
            <Width val="100"/>
            <Height val="84"/>
            <Frame type="Image" name="ChargedTrait">
                <Anchor relative="$parent"/>
                <Texture val="@UI/Storm_UI_HUD_HeroMechanic_Thrall_Charge3"/>
            </Frame>
        </Frame>
        
        <Frame type="Frame" name="ThrallChargeFrame2" template="Thrall/ThrallTraitCounterFrame">
            <Anchor side="Top" relative="$parent/ThrallTraitIcon" pos="Min" offset="-18"/>
            <Anchor side="Left" relative="$parent/ThrallTraitIcon" pos="Min" offset="-18"/>
            <Width val="104"/>
            <Height val="116"/>
            <Frame type="Image" name="ChargedTrait">
                <Anchor relative="$parent"/>
                <Texture val="@UI/Storm_UI_HUD_HeroMechanic_Thrall_Charge2"/>
            </Frame>
        </Frame>
        
        <Frame type="Frame" name="ThrallChargeFrame1" template="Thrall/ThrallTraitCounterFrame">
            <Anchor side="Top" relative="$parent/ThrallTraitIcon" pos="Min" offset="-10"/>
            <Anchor side="Left" relative="$parent/ThrallTraitIcon" pos="Min" offset="34"/>
            <Width val="48"/>
            <Height val="56"/>
            <Frame type="Image" name="ChargedTrait">
                <Anchor relative="$parent"/>
                <Texture val="@UI/Storm_UI_HUD_HeroMechanic_Thrall_Charge1"/>
            </Frame>
        </Frame>

        <Frame type="Frame" name="SparkleAnchor">
            <Anchor side="Top" relative="$parent/ThrallTraitIcon" pos="Min" offset="45"/>
            <Anchor side="Left" relative="$parent/ThrallTraitIcon" pos="Min" offset="46"/>
        </Frame>
        
        <Frame type="Image" name="MrSparkle">
            <Anchor side="Top" relative="$parent/SparkleAnchor" pos="Mid" offset="0"/>
            <Anchor side="Left" relative="$parent/SparkleAnchor" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent/SparkleAnchor" pos="Mid" offset="0"/>
            <Anchor side="Bottom" relative="$parent/SparkleAnchor" pos="Mid" offset="0"/>
            <SubpixelRendering val="True"/>
            <Texture val="@UI/Storm_UI_HUD_HeroMechanic_Thrall_TinySparkle"/>
            <BlendMode val="Add"/>
            <Animation name="SparkleMotion">
                <Event event="OnMouseUp" action="Reset" frame="$parent/ThrallTraitIcon"/>
                <Event event="OnMouseUp" action="Play" frame="$parent/ThrallTraitIcon"/>
                <Event event="OnMouseUp" action="DirectionForward" frame="$parent/ThrallTraitIcon"/>
                <Event event="OnShown" action="Play"/>
                <Event event="OnShown" action="DirectionForward"/>
                <Event event="OnHide" action="Play"/>
                <Event event="OnHide" action="DirectionReverse"/>
                <Controller type="Fade" end="Loop">
                    <Key type="Curve" time="0" value="0" out="Slow"/>
                    <Key type="Curve" time=".3" value="255" in="Slow"/>
                    <Key type="Curve" time="1" value="255" in="Slow"/>
                    <Key type="Curve" time="1.2" value="0" in="Slow"/>
                    <Key type="Curve" time="15" value="0" in="Slow"/>
                </Controller>
                <Controller type="Anchor" end="Loop" side="Left" relative="true" frame="$parent/SparkleAnchor">
                    <Key type="Curve" time="0.0" value="-15" out="Slow"/>
                    <Key type="Curve" time="0.5" value="14" out="Slow"/>
                    <Key type="Curve" time="0.55" value="15" out="Slow"/>
                    <Key type="Curve" time="0.7" value="17" out="Slow"/>
                    <Key type="Curve" time="1.0" value="38" in="Slow"/>
                    <Key type="Curve" time="15" value="38" in="Slow"/>
                </Controller>
                <Controller type="Anchor" end="Loop" side="Top" relative="true" frame="$parent/SparkleAnchor">
                    <Key type="Curve" time="0.0" value="3" out="Slow"/>
                    <Key type="Curve" time="0.5" value="-5" out="Slow"/>
                    <Key type="Curve" time="0.55" value="-15" out="Slow"/>
                    <Key type="Curve" time="0.85" value="2" out="Slow"/>
                    <Key type="Curve" time="1.0" value="10" in="Slow"/>
                    <Key type="Curve" time="15" value="10" in="Slow"/>
                </Controller>
                <Controller type="Rotation" relative="False" end="Loop" >
                    <Key type="Curve" time="0" value="-45" in="linear" out="linear"/>
                    <Key type="Curve" time="0.4" value="4" out="Slow"/>
                    <Key type="Curve" time=".6" value="98" in="slow" out="linear"/>
                    <Key type="Curve" time="1" value="125" in="slow" out="linear"/>
                    <Key type="Curve" time="1.2" value="195" in="slow" out="linear"/>
                    <Key type="Curve" time="15" value="195" in="slow" out="linear"/>
                </Controller>
                <Controller type="Dimension" dimension="Width" relative="False" percentage="True" end="Loop" >
                    <Key type="Curve" time="0" value="0" out="Slow"/>
                    <Key type="Curve" time=".3" value="100" in="Slow"/>
                    <Key type="Curve" time=".5" value="40" in="Slow"/>
                    <Key type="Curve" time=".55" value="10" in="Slow"/>
                    <Key type="Curve" time=".8" value="100" in="Slow"/>
                    <Key type="Curve" time="1" value="50" in="Slow"/>
                    <Key type="Curve" time="15" value="0" in="Slow"/>
                </Controller>
                <Controller type="Dimension" dimension="Height" relative="False" percentage="True" end="Loop" >
                    <Key type="Curve" time="0" value="0" out="Slow"/>
                    <Key type="Curve" time=".3" value="100" in="Slow"/>
                    <Key type="Curve" time=".5" value="40" in="Slow"/>
                    <Key type="Curve" time=".55" value="10" in="Slow"/>
                    <Key type="Curve" time=".8" value="100" in="Slow"/>
                    <Key type="Curve" time="1" value="50" in="Slow"/>
                    <Key type="Curve" time="15" value="0" in="Slow"/>
                </Controller>
            </Animation>
        </Frame>

        <Frame type="Image" name="ActivateIconAnchor">
            <Anchor side="Top" relative="$parent/ThrallTraitIcon" pos="Min" offset="58"/>
            <Anchor side="Left" relative="$parent/ThrallTraitIcon" pos="Min" offset="56"/>
            <Width val="5"/>
            <Width val="5"/>
        </Frame>
        
        <Frame type="Image" name="ActivateIconBase">
            <Width val="75"/>
            <Height val="78"/>
            <Anchor side="Top" relative="$parent/ActivateIconAnchor" pos="Mid" offset="0"/>
            <Anchor side="Bottom" relative="$parent/ActivateIconAnchor" pos="Mid" offset="0"/>
            <Anchor side="Left" relative="$parent/ActivateIconAnchor" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent/ActivateIconAnchor" pos="Mid" offset="0"/>
            <Texture val="@UI/Storm_UI_HUD_HeroMechanic_Thrall_Activate"/>
            <Alpha val="0"/>
            <Animation name="Activate">
                <Event event="TransitionToTop" action="Reset" frame="$parent"/>
                <Event event="TransitionToTop" action="Play" frame="$parent"/>
                <Event event="TransitionToTop" action="DirectionForward" frame="$parent"/>
                <Controller type="Fade" end="Pause" >
                     <Key type="Curve" time="0" value="0" out="Fast"/>
                     <Key type="Curve" time=".2" value="255" in="Fast"/>
                     <Key type="Curve" time=".9" value="0" in="Slow"/>
                 </Controller>
            </Animation>
        </Frame> 

        <Frame type="Image" name="ActivateIconAdd">
            <Anchor relative="$parent/ActivateIconBase"/>
            <Texture val="@UI/Storm_UI_HUD_HeroMechanic_Thrall_Activate"/>
            <BlendMode val="Add"/>
            <Alpha val="0"/>
            <Animation name="Activate">
                <Event event="TransitionToTop" action="Reset" frame="$parent"/>
                <Event event="TransitionToTop" action="Play" frame="$parent"/>
                <Event event="TransitionToTop" action="DirectionForward" frame="$parent"/>
                <Controller type="Dimension" dimension="Width" relative="False" percentage="True" end="Pause" >
                    <Key type="Curve" time="0" value="110" out="fast"/>
                    <Key type="Curve" time=".6" value="200" in="slow" out="slow"/>
                    <Key type="Curve" time=".9" value="520" in="slow"/>
                </Controller>
                <Controller type="Dimension" dimension="Height" relative="False" percentage="True" end="Pause" >
                    <Key type="Curve" time="0" value="110" out="fast"/>
                    <Key type="Curve" time=".6" value="200" in="slow" out="slow"/>
                    <Key type="Curve" time=".9" value="520" in="slow"/>
                </Controller>
                <Controller type="Fade" end="Pause" >
                     <Key type="Curve" time="0" value="255" out="Slow"/>
                     <Key type="Curve" time=".5" value="0" in="Slow"/>
                     <Key type="Curve" time=".9" value="0" in="Slow"/>
                 </Controller>
            </Animation>
        </Frame> 


        
    </Frame>

</Desc>
