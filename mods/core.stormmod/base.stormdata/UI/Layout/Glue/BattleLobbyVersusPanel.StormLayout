<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Desc>
    <!--
        How to adjust roadmap animations:
        RoadmapItem base size = RI
        RoadmapItem spacing = RS
        ContainerFrame Size = CS

        These are the only 3 variables...the rest of the values can be arrived at using them.

        RoadmapItem base offset (top anchored to mid) = -(RI / 2)
        RoadmapItem activation top anchor offset delta (to work around anchoring bug? is a relative animation) = -(CS - RI) / 2

        AnchorFrame Initial offset = -(CS / 2)
        AnchorFrame Transition offset = (RI + RS)

        ## Just including these for completeness, but these constants aren't defined.
        RoadmapItem size animation inactive value = RI
        RoadmapItem size animation active value = CS

        Fill out these constants with the final results of the above formulas and the animations should all work out
    -->

    <Constant name="RoadmapItemSize" val="20"/>
    <Constant name="RoadmapItemSpacing" val="5"/>
    <Constant name="RoadmapContainerFrameSize" val="136"/>
    <Constant name="RoadmapItemInactiveTopOffset" val="-10"/>
    <Constant name="RoadmapItemActiveTopOffset" val="-58"/>
    <Constant name="RoadmapAnchorFrameInitialOffset" val="-68"/>
    <Constant name="RoadmapAnchorFrameTransitionOffset" val="-25"/>

    <Frame type="HeroDraftRoadmapItem" name="HeroDraftRoadmapItemTemplate">
        <Anchor side="Top" relative="$parent" pos="Mid" offset="#RoadmapItemInactiveTopOffset"/>
        <Height val="#RoadmapItemSize"/>
        <Width val="#RoadmapItemSize"/>
        <AcceptsMouseTooltip val="true"/>

        <StateGroup name="DraftRoundType">
            <DefaultState val="BlueTeamPick"/>

            <State name="TeamBan">
                <When type="AnimationState" frame="$this" AnimStateActiveTeam="Ban"/>
                <Action type="SetProperty" frame="RoundTypeImage" AdjustmentColor="60386b"/>
            </State>

            <State name="RedTeamPick">
                <When type="AnimationState" frame="$this" AnimStateActiveTeam="TeamRed"/>
                <Action type="SetProperty" frame="RoundTypeImage" AdjustmentColor="7e0018"/>
            </State>

            <State name="BlueTeamPick">
                <When type="AnimationState" frame="$this" AnimStateActiveTeam="TeamBlue"/>
                <Action type="SetProperty" frame="RoundTypeImage" AdjustmentColor="173d94"/>
            </State>
        </StateGroup>

        <StateGroup name="ItemValidity">
            <DefaultState val="InvalidState" />

            <State name="ValidState">
                <When type="AnimationState" frame="$this" AnimStateValidItem="Valid"/>
                <Action type="SetProperty" frame="$this" visible="true"/>
            </State>

            <State name="InvalidState">
                <Action type="SetProperty" frame="$this" visible="false"/>
            </State>
        </StateGroup>        

        <Animation name="ActivationAnimations">
            <Event event="Active" action="DirectionForward,Play"/>
            <Event event="Inactive" action="DirectionReverse,Play"/>

            <Controller type="Dimension" dimension="Width" end="Pause">
                <Key type="Curve" time="0" value="#RoadmapItemSize" out="Slow"/>
                <Key type="Curve" time=".2" value="#RoadmapContainerFrameSize" in="Slow"/> 
            </Controller>

            <Controller type="Dimension" dimension="Height" end="Pause">
                <Key type="Curve" time="0" value="#RoadmapItemSize" out="Slow"/>
                <Key type="Curve" time=".2" value="#RoadmapContainerFrameSize" in="Slow"/> 
            </Controller>

            <Controller type="Anchor" end="Pause" side="Top" relative="true">
                <Key type="Curve" time="0" value="0" out="Slow"/>
                <Key type="Curve" time=".2" value="#RoadmapItemActiveTopOffset" in="Slow"/>
            </Controller>

            <Controller type="Fade" end="Pause">
                <Key type="Curve" time="0" value="255" inout="Slow"/>
                <Key type="Curve" time=".2" value="0" inout="Slow"/>
            </Controller>
        </Animation>

        <Frame type="Image" name="RoundTypeImage">
            <Anchor relative="$parent" />
            <Texture val="@UI/storm_ui_glues_draft_hero_team_turn_pip"/>
            <ColorAdjustMode val="Colorize"/>
            <SubpixelRendering val="true"/>
        </Frame>
    </Frame>

    <Frame type="Image" name="RoundActivityIndicatorTemplate">
        <LayerCount val="2"/>
        <Texture val="@UI/storm_ui_glues_draft_hero_team_turn_arrows" Layer="1"/>
        <Texture val="@UI/storm_ui_glues_draft_hero_team_turn_mask" Layer="0"/>
        <AlphaLayer val="True" Layer="0"/>
        <WrapUV val="true" Layer="1"/>
        <ColorAdjustMode val="Colorize"/>
        <Visible val="false"/>
    </Frame>

    <Frame type="BattleLobbyTeamPanel" name="VersusPanelTeamTemplate">
        <Width val="363"/>
        <Height val="100"/>

        <Animation name="Start">
            <Event event="Start" action="Restart,DirectionForward,Play" frame="$parent"/>
            <Controller type="Anchor" side="Top" relative="true" end="Pause">
                <Key type="Curve" time="0" value="-120" out="Slow"/>
                <Key type="Curve" time="0.8" value="-120" out="Fast"/>
                <Key type="Curve" time="1.6" value="0" in="Slow"/>
            </Controller>

            <Controller type="Fade" end="Pause">
                <Key type="Curve" time="0" value="0" out="Slow"/>
                <Key type="Curve" time=".8" value="0" out="Slow"/>
                <Key type="Curve" time="1.6" value="255" in="Slow"/>
            </Controller>
        </Animation>
    </Frame>

    <!-- Draft Mode Blue VS Red Header -->
    <Frame type="BattleLobbyVersusPanel" name="DraftHeaderTemplate">
        <TimerProgressFills val="false"/>

        <Visible val="false"/>

        <Animation name="Start">
            <Event event="Show" action="Reset,Play"/>
            <Event event="Reset" action="Stop"/>

            <Controller type="Anchor" end="Pause" side="Top" relative="true" frame="$this">
                <Key type="Curve" time="0" value="-160" out="Fast"/>
                <Key type="Curve" time=".8" value="0" in="Slow"/>
            </Controller>
            
            <Controller type="Event" end="Stop" >
                <Key type="Event" time="0.0" event="Null"/>
                <Key type="Event" time="1.0" event="Start"/>
            </Controller>
        </Animation>

        <Animation name="FirstRoundStarted">
            <Event event="DraftPickBegin" action="Play" frame="$parent" />
            <Event event="OnHidden" action="Stop"/>

            <Controller type="Fade" end="Pause" frame="$this/CountdownContainer">
                <Key type="Curve" time="0" value="0" out="Fast"/>
                <Key type="Curve" time=".5" value="255" in="Fast"/>
            </Controller>

            <Controller type="Fade" end="Pause" frame="$this/Roadmap">
                <Key type="Curve" time="0" value="0" out="Fast"/>
                <Key type="Curve" time=".5" value="255" in="Fast"/>
            </Controller>

            <Controller type="Anchor" end="Pause" frame="$this/Roadmap/AnchorFrame" side="Left" relativeFrame="$this/RoadMap" pos="Mid">
                <Key type="Curve" time="0" value="#RoadmapAnchorFrameInitialOffset"/>
            </Controller>
        </Animation>

        <Frame type="Label" name="MapNameLabel">
            <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
            <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
            <Style val="HeroDraftMapName"/>
        </Frame>

        <Frame type="Frame" name="CountdownContainer">
            <Anchor side="Top" relative="$parent/MapNameLabel" pos="Max" offset="-7"/>
            <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>

            <Width val="#RoadmapContainerFrameSize"/>
            <Height val="#RoadmapContainerFrameSize"/>
            <Alpha val="0"/>

            <Frame type="Image" name="TeamBG">
                <Anchor relative="$parent" />

                <Animation name="TeamRedPick">
                    <Event event="TeamRedPick" action="Restart,Play" frame="$parent/$parent"/>

                    <Controller type="Texture" end="Pause">
                        <Key type="Image" time="0.0" image="@UI/storm_ui_glues_draft_hero_team_turn_bg_red"/>
                    </Controller>                    
                </Animation>

                <Animation name="TeamBlueActivity">
                    <Event event="TeamBluePick" action="Restart,Play" frame="$parent/$parent"/>

                    <Controller type="Texture" end="Pause">
                        <Key type="Image" time="0.0" image="@UI/storm_ui_glues_draft_hero_team_turn_bg_blue"/>
                    </Controller>                    
                </Animation>

                <Animation name="TeamBans">
                    <Event event="TeamRedBan" action="Restart,Play" frame="$parent/$parent"/>
                    <Event event="TeamBlueBan" action="Restart,Play" frame="$parent/$parent"/>
                    <Event event="TeamNone" action="Restart,Play" frame="$parent/$parent"/>

                    <Controller type="Texture" end="Pause">
                        <Key type="Image" time="0.0" image="@UI/storm_ui_glues_draft_hero_team_turn_bg_purple"/>
                    </Controller>                    
                </Animation>
            </Frame>            

            <Frame type="Image" name="RoundActivityIndicatorBlue" template="BattleLobbyVersusPanel/RoundActivityIndicatorTemplate">
                <Anchor relative="$parent" />
                <AdjustmentColor val="173d94"/>
                <BlendMode val="Add"/>

                <Animation name="TeamBlueActivityVisibility">
                    <Event event="TeamBluePick" action="Restart,Play" frame="$parent/$parent"/>
                    <Event event="TeamBlueBan" action="Restart,Play" frame="$parent/$parent"/>
                    <Event event="TeamRedPick" action="Stop" frame="$parent/$parent"/>
                    <Event event="TeamRedBan" action="Stop" frame="$parent/$parent"/>
                    <Event event="TeamNone" action="Stop" frame="$parent/$parent"/>

                    <Controller type="Visibility" end="Pause">
                        <Key type="Bool" time="0" value="True"/>
                    </Controller>
                </Animation>

                <Animation name="TeamBlueArrow">
                    <Event event="OnShown" action="Play"/>

                    <Controller type="LayerUV" layer="1" side="Left" end="Loop">
                        <Key type="Curve" time="0" value="0" out="fast"/>
                        <Key type="Curve" time="1" value="1" in="fast"/>
                    </Controller>
                </Animation>

                <Animation name="TeamBlueBanArrow">
                    <Event event="TeamBlueBan" action="Play"/>

                    <Controller type="Color" end="Pause">
                        <Key type="Curve" time="0" value="128,41,169" out="Fast"/>
                    </Controller>
                </Animation>
            </Frame>

            <Frame type="Image" name="RoundActivityIndicatorRed" template="BattleLobbyVersusPanel/RoundActivityIndicatorTemplate">
                <Anchor relative="$parent" />
                <TextureCoords top="0" left="1" bottom="1" right="0" layer="1"/>
                <AdjustmentColor val="7e0018"/>
                <BlendMode val="Add"/>

                <Animation name="TeamRedActivityVisibility">
                    <Event event="TeamRedPick" action="Restart,Play" frame="$parent/$parent"/>
                    <Event event="TeamRedBan" action="Restart,Play" frame="$parent/$parent"/>
                    <Event event="TeamBluePick" action="Stop" frame="$parent/$parent"/>
                    <Event event="TeamBlueBan" action="Stop" frame="$parent/$parent"/>
                    <Event event="TeamNone" action="Stop" frame="$parent/$parent"/>

                    <Controller type="Visibility" end="Pause">
                        <Key type="Bool" time="0" value="True" out="fast"/>
                    </Controller>
                </Animation>

                <Animation name="TeamRedArrow">
                    <Event event="OnShown" action="Play"/>

                    <Controller type="LayerUV" layer="1" side="Left" end="Loop">
                        <Key type="Curve" time="0" value="0" out="fast"/>
                        <Key type="Curve" time="1" value="1" in="fast"/>
                    </Controller>
                </Animation>
            </Frame>

            <Frame type="ProgressBar" name="TimerBar">
                <Anchor relative="$parent" />

                <MinValue val="0"/>
                <MaxValue val="1"/>
                <Circular val="true"/>
                <CircularStart val="0"/>
                <CircularEndOffset val="360"/>
                <FillInset val="0.0"/>
        
                <Frame type="Frame" name="FillImageContainer">
                    <Anchor relative="$parent"/>

                    <Animation name="TeamRedPick">
                        <Event event="TeamRedPick" action="Restart,Play" frame="$parent/$parent/$parent"/>

                        <Controller type="Texture" end="Pause" frame="$this/FillImage">
                            <Key type="Image" time="0.0" image="@UI/storm_ui_glues_draft_hero_team_turn_countdown_red"/>
                        </Controller>
                    </Animation>

                    <Animation name="TeamBlueActivity">
                        <Event event="TeamBluePick" action="Restart,Play" frame="$parent/$parent/$parent"/>

                        <Controller type="Texture" end="Pause" frame="$this/FillImage">
                            <Key type="Image" time="0.0" image="@UI/storm_ui_glues_draft_hero_team_turn_countdown_blue"/>
                        </Controller>
                    </Animation>

                    <Animation name="TeamBans">
                        <Event event="TeamRedBan" action="Restart,Play" frame="$parent/$parent/$parent"/>
                        <Event event="TeamBlueBan" action="Restart,Play" frame="$parent/$parent/$parent"/>
                        <Event event="TeamNone" action="Restart,Play" frame="$parent/$parent/$parent"/>

                        <Controller type="Texture" end="Pause" frame="$this/FillImage">
                            <Key type="Image" time="0.0" image="@UI/storm_ui_glues_draft_hero_team_turn_countdown_purple"/>
                        </Controller>
                    </Animation>
                        
                    <Frame type="Image" name="FillImage">
                        <Anchor relative="$parent"/>
                        <LayerCount val="1"/>
                        <TextureType layer="0" val="Circular"/>
                    </Frame>
                </Frame>
            </Frame>

            <Frame type="Label" name="Label">
                <Anchor side="Top" relative="$parent" pos="Min" offset="29"/>
                <Anchor side="Left" relative="$parent" pos="Mid" offset="1"/>
                <Anchor side="Right" relative="$parent" pos="Mid" offset="1"/>

                <Style val="HeroDraftHeaderVersusBlue"/>
 
                <Animation name="CountdownStarted">
                    <Event event="CountdownStarted" action="Reset,Play" frame="$parent/$parent"/>
                    <Event event="CountdownBanStarted" action="Reset,Play" frame="$parent/$parent"/>
                    <Event event="CountdownSwapStarted" action="Reset,Play" frame="$parent/$parent"/>

                    <Controller type="Fade" end="Pause">
                        <Key type="Curve" time="0" value="255" out="Slow"/>
                    </Controller>
                </Animation>

                <Animation name="TeamRedPick">
                    <Event event="TeamRedPick" action="Reset,Play" frame="$parent/$parent"/>

                    <Controller type="Style" end="Pause" style1="HeroDraftHeaderVersusRed" style2="HeroDraftHeaderVersusRed">
                        <Key type="Curve" time="0" value="1" out="Auto"/>
                    </Controller>               
                </Animation>

                <Animation name="TeamBlueActivity">
                    <Event event="TeamBluePick" action="Reset,Play" frame="$parent/$parent"/>

                    <Controller type="Style" end="Pause" style1="HeroDraftHeaderVersusBlue" style2="HeroDraftHeaderVersusBlue">
                        <Key type="Curve" time="0" value="1" out="Auto"/>
                    </Controller>                   
                </Animation>

                <Animation name="TeamBans">
                    <Event event="TeamRedBan" action="Reset,Play" frame="$parent/$parent"/>
                    <Event event="TeamBlueBan" action="Reset,Play" frame="$parent/$parent"/>
                    <Event event="TeamNone" action="Reset,Play" frame="$parent/$parent"/>

                    <Controller type="Style" end="Pause" style1="HeroDraftHeaderVersusBan" style2="HeroDraftHeaderVersusBan">
                        <Key type="Curve" time="0" value="1" out="Auto"/>
                    </Controller>                
                </Animation>
            
                <Animation name="CountdownWarning">
                    <Event event="CountdownWarning" action="Reset,Play" frame="$parent/$parent"/>
                    <Event event="CountdownBanWarning" action="Reset,Play" frame="$parent/$parent"/>
                    
                    <Controller type="Style" end="Pause" style1="HeroDraftHeaderVersusWarning" style2="HeroDraftHeaderVersusWarning">
                        <Key type="Curve" time="0" value="1.0" out="Auto"/>
                    </Controller>
                </Animation>
            
                <Animation name="CountdownCritical">
                    <Event event="CountdownCritical" action="Reset,Play" frame="$parent/$parent"/>
                    <Event event="CountdownBanCritical" action="Reset,Play" frame="$parent/$parent"/>

                    <Controller type="Style" end="Pause" style1="HeroDraftHeaderVersusCritical" style2="HeroDraftHeaderVersusCritical">
                        <Key type="Curve" time="0.00" value="1.0" out="Auto"/>
                    </Controller>
                </Animation>
            
                <Animation name="CountdownFinished">
                    <Event event="CountdownFinished" action="Restart,Play" frame="$parent/$parent"/>
                    <Event event="CountdownBanFinished" action="Restart,Play" frame="$parent/$parent"/>

                    <Controller type="Fade" end="Pause">
                        <Key type="Curve" time="0" value="255" out="Slow"/>
                    </Controller>
                </Animation>
            </Frame>
        </Frame>

        <Frame type="HeroDraftRoadmap" name="Roadmap">
            <Anchor side="Top" relative="$parent/CountdownContainer" pos="Mid" offset="-50"/>
            <Anchor side="Bottom" relative="$parent/CountdownContainer" pos="Mid" offset="50"/>            
            <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
            <Alpha val="0"/>
            <DraftRoadmapItemTemplate val="BattleLobbyVersusPanel/HeroDraftRoadmapItemTemplate"/>
            <ItemSpacing val="#RoadmapItemSpacing"/>

            <Frame type="Frame" name="AnchorFrame">
                <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
                <Anchor side="Top" relative="$parent" pos="Mid" offset="0"/>
                <Anchor side="Right" relative="$this" pos="Min" offset="0"/>
                <Anchor side="Bottom" relative="$parent" pos="Mid" offset="0"/>

                <Animation name="RoundTransition">
                    <Event event="AdvanceToNextDraftRound" action="RefreshBaseValue,Restart,Play" frame="$parent"/>

                    <Controller type="Anchor" end="Pause" side="Left" relative="true">
                        <Key type="Curve" time="0" value="0" out="Slow"/>
                        <Key type="Curve" time=".2" value="#RoadmapAnchorFrameTransitionOffset" in="Slow"/>
                    </Controller>
                </Animation>
            </Frame>
        </Frame>

        <Frame type="BattleLobbyTeamPanel" name="Team0" template="BattleLobbyVersusPanel/VersusPanelTeamTemplate">
            <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="0"/>
        </Frame>

        <Frame type="BattleLobbyTeamPanel" name="Team1" template="BattleLobbyVersusPanel/VersusPanelTeamTemplate">
            <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="0"/>
        </Frame>
    </Frame>
</Desc>