<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Desc>
    <DescFlags val="Locked"/>

    <!-- ==========================================================
        Queue Requirements Explanation Tooltip
    =========================================================== -->
    <Frame type="AnchoredPopup" name="UnrankedDraftReadyButtonAnchoredPopupTemplate" template="StandardPopupTooltip/ReadyButtonAnchoredPopupTemplate">
        <StateGroup name="ReadyEnabled">
            <State name="Hide_ButtonEnabled">
                <When type="Property" frame="$ScreenPlay_HeroUnrankedDraftPanel" ReadyAllowed="true" />
                <Action type="SendEvent" frame="$this" event="HideRequirements" />
            </State>
            <State name="Hide_PanelDisabled">
                <When type="Property" frame="$ScreenPlay_HeroUnrankedDraftPanel" Visible="false" />
                <Action type="SendEvent" frame="$this" event="HideRequirements" />
            </State>
            <State name="Suppressed">
                <When type="Property" frame="$ScreenPlay_HeroUnrankedDraftPanel" ReadyRequirementsPopupSuppressed="true" />
                <Action type="SendEvent" frame="$this" event="HideRequirements" />
            </State>
            <State name="Show">
                <When type="Property" frame="$ScreenPlay_HeroUnrankedDraftPanel" Visible="true" />
                <When type="Property" frame="$ScreenPlay_HeroUnrankedDraftPanel" ReadyAllowed="false" />
                <Action type="SendEvent" frame="$this" event="ShowRequirements" />
            </State>
        </StateGroup>

        <Frame type="Button" name="CloseButton">
            <Animation name="OnShown">
                <Controller type="Property" name="Suppress" frame="$ScreenPlay_HeroUnrankedDraftPanel" property="ReadyRequirementsPopupSuppressed" end="Pause">
                    <Key type="Property" time="0.00" value="true"/>
                </Controller>
            </Animation>
        </Frame>
        
        <Animation name="ReadyEnabledShow">
            <Event event="ShowRequirements" action="DirectionForward,Play"/>
            <Event event="HideRequirements" action="DirectionReverse,Play"/>

            <Controller type="Property" property="Show" end="Pause">
                <Key type="Property" time="0.00" value="false"/>
                <Key type="Property" time="0.24" value="false"/>
                <Key type="Property" time="0.25" value="true"/>
            </Controller>
        </Animation>

        <Animation name="TransitionOut">
            <Event event="Hide" frame="$ScreenPlay_HeroUnrankedDraftPanel" action="Restart,Play"/>
            <Controller type="Event" end="Stop">
                <Key type="Event" time="0" event="Hide" />
            </Controller>
        </Animation>

        <Animation name="TransitionIn">
            <Event event="Show" frame="$ScreenPlay_HeroUnrankedDraftPanel" action="Restart,Play"/>
            <Controller type="Fade" end="Pause">
                <Key type="Curve" time="0.0" value="0" out="Slow"/>
                <Key type="Curve" time="0.4" value="255" in="Slow" out="Slow"/>
            </Controller>
        </Animation>

        <Frame type="PropertyBind" name="PropertyBindTitle">
            <Anchor relative="$parent"/>
            <Source frame="$ScreenPlay_HeroUnrankedDraftPanel" property="ReadyRequirementsTitle"/>
            <Target frame="$parent/Container/TitleLabel" property="Text"/>
        </Frame>

        <Frame type="PropertyBind" name="PropertyBindText">
            <Anchor relative="$parent"/>
            <Source frame="$ScreenPlay_HeroUnrankedDraftPanel" property="ReadyRequirementsText"/>
            <Target frame="$parent/Container/BodyLabel" property="Text"/>
        </Frame>
    </Frame>

    <!-- ==========================================================
        Main Frame
    =========================================================== -->
    <Frame type="HeroUnrankedDraftPanel" name="HeroUnrankedDraftPanel">
        <Anchor relative="$parent"/>

        <!-- ==========================================================
            Transition Effects
            Triggered when going to/from this tab panel
        =========================================================== -->
        <Animation name="Transition">
            <Event event="Show" action="Reset,Play"/>
            <Event event="Hide" action="DirectionReverse,Play"/>
            <Controller type="Fade" end="Pause">
                <Key type="Curve" time="0.0" value="0" out="Slow"/>
                <Key type="Curve" time="0.4" value="255" in="Slow" out="Slow"/>
            </Controller>
        </Animation>

        <Animation name="TransitionSound">
            <Event event="Show" action="Reset,Play"/>
            <Controller type="Sound" end="Pause">
                <Key type="Sound" time="0.0" sound="@UI_Gluescreen_Transition_HeroLeague"/>
            </Controller>
        </Animation>

        <!-- ==========================================================
            Ready Allowed states
            Used to show/hide locked/unlocked states
        =========================================================== -->
        <StateGroup name="QualificationState">
            <DefaultState val="NotAllowed"/>

            <State name="NotAllowed">
                <When type="Property" ReadyAllowed="false" />
                <Action type="SendEvent" frame="$this" event="ShowDisqualification" />
            </State>

            <State name="Allowed">
                <When type="Property" ReadyAllowed="true" />
                <Action type="SendEvent" frame="$this" event="HideDisqualification" />
            </State>
        </StateGroup>

        <Animation name="QualificationStateShow">
            <Event event="ShowDisqualification" action="DirectionForward,Play"/>
            <Event event="HideDisqualification" action="DirectionReverse,Play"/>

            <Controller type="Property" property="Visible" end="Pause" frame="LockedContainer">
                <Key type="Property" time="0.00" value="false"/>
                <Key type="Property" time="0.24" value="false"/>
                <Key type="Property" time="0.25" value="true"/>
            </Controller>

            <Controller type="Property" property="Visible" end="Pause" frame="UnlockedImage">
                <Key type="Property" time="0.00" value="true"/>
                <Key type="Property" time="0.24" value="true"/>
                <Key type="Property" time="0.25" value="false"/>
            </Controller>
        </Animation>

        <!-- ==========================================================
            Searching/Not Searching states
            Used to set animations on the screen when searching starts
        =========================================================== -->
        <StateGroup name="SearchingQueueState">
            <DefaultState val="NotSearching"/>

            <State name="NotSearching">
                <When type="Property" IsSearchingQueue="false" />
                <Action type="SendEvent" frame="UnlockedImage" event="IdleSpinning" />
            </State>

            <State name="Searching">
                <When type="Property" IsSearchingQueue="true" />
                <Action type="SendEvent" frame="UnlockedImage" event="ReadySpinning" />
            </State>
        </StateGroup>

        <!-- ==========================================================
            Title
        =========================================================== -->
        <Frame type="Image" name="UnrankedVignette">
            <Anchor side="Top" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="0"/>
            <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
            <Texture val="@UI/Storm_Lobby_Unranked_Vignette"/>
        </Frame>

        <Frame type="Label" name="TitleLabel">
            <Anchor side="Top" relative="$parent" pos="Min" offset="200"/>
            <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
            <Style val="HeroLobbyRankedTitle"/>
            <Text val="@UI/UnrankedDraft"/>
        </Frame>

        <Frame type="Image" name="TitleBackgroundImage">
            <Anchor side="Top" relative="$parent/TitleLabel" pos="Mid" offset="20"/>
            <Anchor side="Bottom" relative="$parent/TitleLabel" pos="Mid" offset="20"/>
            <Anchor side="Left" relative="$parent/TitleLabel" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent/TitleLabel" pos="Mid" offset="0"/>
            <Texture val="@UI/Storm_Lobby_Ranked_HeroLeague_RankBG_Glow"/>
            <Width val="1250"/>
            <Height val="208"/>
            <BlendMode val="Add"/>
            <Alpha val="175"/>
        </Frame>

        <Frame type="Label" name="DescriptionLabel">
            <Anchor side="Top" relative="$parent/TitleLabel" pos="Max" offset="10"/>
            <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
            <Style val="HeroLobbyRankedTitleDescription"/>
            <Text val="@UI/UnrankedDraft/Description"/>
        </Frame>

        <!-- ==========================================================
            Bottom Floor Image
        =========================================================== -->
        <Frame type="Image" name="UnrankedFloorBg">
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="0"/>
            <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
            <Texture val="@UI/Storm_Lobby_Unranked_FloorBg"/>
            <RenderType val="HDR"/>
            <RenderPriority val="300"/>
        </Frame>

        <!-- ==========================================================
            Center Image (lock or runes)
        =========================================================== -->
        <Frame type="Frame" name="LockedContainer">
            <Anchor relative="$parent"/>
            <Visible val="false"/>

            <Frame type="Image" name="LockedImageRingShadow">
                <Anchor side="Top" relative="$parent" pos="Mid" offset="0"/>
                <Anchor side="Bottom" relative="$parent" pos="Mid" offset="0"/>
                <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
                <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
                <Texture val="@UI/Ranked_Badge_Locked_Ring_Shadow"/>
                <Width val="1300"/>
                <Height val="600"/>
                <BlendMode val="Add"/>
            </Frame>

            <Frame type="Image" name="LockedImageRingGlow1">
                <Anchor side="Top" relative="$parent" pos="Mid" offset="5"/>
                <Anchor side="Bottom" relative="$parent" pos="Mid" offset="5"/>
                <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
                <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
                <Texture val="@UI/Ranked_Badge_Placement_Promotion_Glow"/>
                <Width val="760"/>
                <Height val="760"/>
                <BlendMode val="Add"/>
            </Frame>

            <Frame type="Image" name="LockedImageRingGlow2">
                <Anchor side="Top" relative="$parent" pos="Mid" offset="30"/>
                <Anchor side="Bottom" relative="$parent" pos="Mid" offset="30"/>
                <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
                <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
                <Texture val="@UI/Ranked_Badge_Placement_Promotion_Glow"/>
                <Width val="760"/>
                <Height val="760"/>
                <Rotation val="180"/>
                <BlendMode val="Add"/>
            </Frame>

            <Frame type="Image" name="LockedImageRing">
                <Anchor side="Top" relative="$parent" pos="Mid" offset="17"/>
                <Anchor side="Bottom" relative="$parent" pos="Mid" offset="17"/>
                <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
                <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
                <Texture val="@UI/Ranked_Badge_Locked_Ring"/>
                <Width val="432"/>
                <Height val="432"/>
            </Frame>

            <Frame type="Image" name="LockedImageIcon">
                <Anchor side="Top" relative="$parent/LockedImageRing" pos="Mid" offset="-8"/>
                <Anchor side="Bottom" relative="$parent/LockedImageRing" pos="Mid" offset="-8"/>
                <Anchor side="Left" relative="$parent/LockedImageRing" pos="Mid" offset="-2"/>
                <Anchor side="Right" relative="$parent/LockedImageRing" pos="Mid" offset="-2"/>
                <Texture val="@UI/Ranked_Badge_Locked_Icon"/>
                <Width val="281"/>
                <Height val="281"/>
            </Frame>
        </Frame>

        <Frame type="Image" name="UnlockedImage">
            <Anchor side="Top" relative="$parent" pos="Mid" offset="30"/>
            <Anchor side="Bottom" relative="$parent" pos="Mid" offset="30"/>
            <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
            <Texture val="@UI/Storm_Lobby_Unranked_Rune_Spinner"/>
            <RenderType val="HDR"/>
            <RenderPriority val="800"/>
            <BlendMode val="Add"/>
            <ColorAdjustMode val="AddSelf"/>

            <Animation name="SpinIdle">
                <Event event="IdleSpinning" action="Play"/>
                <!-- <Event event="ReadySpinning" action="Pause"/> -->

                <Controller type="AdjustmentColor" end="Loop">
                    <Key type="Curve" time="0" value="0,255,255,255" out="Auto"/>
                    <Key type="Curve" time="1" value="50,255,255,255" In="Auto" out="Auto"/>
                    <Key type="Curve" time="2" value="0,255,255,255" In="Auto" />
                </Controller>
                <Controller type="Rotation" relative="False" end="Loop">
                    <Key type="Curve" time="0" value="0" out="linear"/>
                    <Key type="Curve" time="50" value="-360" in="linear"/>
                </Controller>
            </Animation>

            <Animation name="SpinReady">
                <Event event="IdleSpinning" action="Play"/>

                <Controller type="AdjustmentColor" end="Loop">
                    <Key type="Curve" time="0" value="0,255,255,255" out="Auto"/>
                    <Key type="Curve" time=".5" value="100,255,255,255" In="Auto" out="Auto"/>
                    <Key type="Curve" time="1" value="0,255,255,255" In="Auto" />
                </Controller>
                <Controller type="Rotation" relative="False" end="Loop">
                    <Key type="Curve" time="0" value="0" out="linear"/>
                    <Key type="Curve" time="50" value="-360" in="linear"/>
                </Controller>
            </Animation>
        </Frame>

        <!-- ==========================================================
            Side Hero Cutscenes
        =========================================================== -->
        <!-- CAUTION: The 3D models of the cutscene extend beyond the left and top of the cutscene, and if not top-left aligned will appear clipped -->
        <Frame type="CutsceneFrame" name="HeroLeftCutscene">
            <Anchor side="Top" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="40"/>
            <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
            <RenderType val="HDR"/>
            <AutoPlay val="true"/>
            <ToneMapping val="true"/>
            <Preload val="false"/>
            <File val="Cutscenes/UI_Unranked_Heroes_Left.StormCutscene"/>

            <Animation name="OnShown">
                <Event event="OnShown" action="Reset"/>
                <Event event="OnShown" action="Play"/>
                <Controller type="Cutscene" end="Stop">
                    <Key type="Cutscene" time="0.0" action="Stop"/>
                    <Key type="Cutscene" time="0.0" action="SetCutscene" file="Cutscenes/UI_Unranked_Heroes_Left.StormCutscene"/>
                    <Key type="Cutscene" time="0.0" action="Play"/>
                    <Key type="Cutscene" time="0.0" action="Bookmark" bookmark="Look Start"/>
                </Controller>
            </Animation>
        </Frame>

        <!-- CAUTION: The 3D models of the cutscene extend beyond the right and top of the cutscene, and if not top-right aligned will appear clipped -->
        <Frame type="CutsceneFrame" name="HeroRightCutscene">
            <Anchor side="Top" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Left" relative="$parent" pos="Min" offset="-100"/>
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="40"/>
            <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
            <RenderType val="HDR"/>
            <AutoPlay val="true"/>
            <ToneMapping val="true"/>
            <Preload val="false"/>
            <File val="Cutscenes/UI_Unranked_Heroes_Right.StormCutscene"/>

            <Animation name="OnShown">
                <Event event="OnShown" action="Reset"/>
                <Event event="OnShown" action="Play"/>
                <Controller type="Cutscene" end="Stop">
                    <Key type="Cutscene" time="0.0" action="Stop"/>
                    <Key type="Cutscene" time="0.0" action="SetCutscene" file="Cutscenes/UI_Unranked_Heroes_Right.StormCutscene"/>
                    <Key type="Cutscene" time="0.0" action="Play"/>
                    <Key type="Cutscene" time="0.0" action="Bookmark" bookmark="Look Start"/>
                </Controller>
            </Animation>
        </Frame>

        <!-- ==========================================================
            Fog Cutscene and Clouds
        =========================================================== -->
        <Frame type="CutsceneFrame" name="FogCutscene">
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="100"/>
            <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
            <Height val="1500"/>
            <RenderType val="HDR"/>
            <AutoPlay val="true"/>
            <Alpha val="70"/>
            <Preload val="false"/>

            <Animation name="OnShown">
                <Event event="OnShown" action="Reset"/>
                <Event event="OnShown" action="Play"/>
                <Controller type="Cutscene" end="Stop">
                    <Key type="Cutscene" time="0.0" action="SetCutscene" file="Cutscenes/UI_GroundFog_Blue.StormCutscene"/>
                    <Key type="Cutscene" time="0.0" action="Bookmark" bookmark="Stand Start"/>
                    <Key type="Cutscene" time="0.0" action="Play"/>
                </Controller>
            </Animation>
        </Frame>

        <Frame type="Image" name="Clouds1Image">
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="0"/>
            <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
            <Height val="936"/>
            <Width val="4000"/>
            <LayerCount val="3"/>
            <SubpixelRendering val="True"/>

            <Texture val="@UI/Storm_UI_Plasma_Clouds2_Mask" layer="0"/>
            <TextureCoords top="0" left="0" bottom="1" right=".5" layer="0"/>
            <AlphaLayer val="True" layer="0"/>
            <WrapUV val="True" layer="0"/>

            <Texture val="@UI/Storm_UI_Clouds_RepeatX" layer="1"/>
            <TextureCoords top="0" left="-.2" bottom="1" right=".8" layer="1"/>
            <AlphaLayer val="True" layer="1"/>
            <WrapUV val="True" layer="1"/>

            <Texture val="@UI/Storm_UI_Clouds_RepeatX" layer="2"/>
            <TextureCoords top="0" left="0" bottom="1" right=".5" layer="2"/>
            <WrapUV val="True" layer="2"/>

            <Animation name="UVScroll">
                <Event event="OnShown" action="Restart" frame="$parent"/>
                <Event event="OnShown" action="Play" frame="$parent"/>
                <Controller type="LayerUV" layer="0" side="Left" relative="True" sidelock="True" end="Loop" >
                    <Key type="Curve" time="0" value="0" out="fast"/>
                    <Key type="Curve" time="91.9" value="1" in="fast"/>
                </Controller>
                <Controller type="LayerUV" layer="1" side="Left" relative="True" sidelock="True" end="Loop" >
                    <Key type="Curve" time="0" value="0" out="fast"/>
                    <Key type="Curve" time="31.9" value="1" in="fast"/>
                </Controller>
            </Animation>
        </Frame>

        <Frame type="Image" name="Clouds2Image">
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="0"/>
            <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
            <Height val="936"/>
            <Width val="4000"/>
            <LayerCount val="3"/>
            <SubpixelRendering val="True"/>
            <Alpha val="255"/>
            <Visible val="True"/>

            <Texture val="@UI/Storm_UI_Clouds_RepeatX" layer="0"/>
            <TextureCoords top="0" left="0" bottom="1" right=".5" layer="0"/>
            <AlphaLayer val="True" layer="0"/>
            <WrapUV val="True" layer="0"/>

            <Texture val="@UI/Storm_UI_Clouds_RepeatX" layer="1"/>
            <TextureCoords top="0" left="-.2" bottom="1" right=".8" layer="1"/>
            <AlphaLayer val="True" layer="1"/>
            <WrapUV val="True" layer="1"/>

            <Texture val="@UI/Storm_UI_Clouds_RepeatX" layer="2"/>
            <TextureCoords top="0" left="1" bottom="1" right="0" layer="2"/>
            <WrapUV val="True" layer="2"/>

            <Animation name="UVScroll">
                <Event event="OnShown" action="Restart" frame="$parent"/>
                <Event event="OnShown" action="Play" frame="$parent"/>
                <Controller type="LayerUV" layer="0" side="Left" relative="True" sidelock="True" end="Loop" >
                    <Key type="Curve" time="0" value="0" out="fast"/>
                    <Key type="Curve" time="121.9" value="1" in="fast"/>
                </Controller>
                <Controller type="LayerUV" layer="1" side="Left" relative="True" sidelock="True" end="Loop" >
                    <Key type="Curve" time="0" value="0" out="fast"/>
                    <Key type="Curve" time="91.9" value="1" in="fast"/>
                </Controller>
            </Animation>
        </Frame>

        <!-- ==========================================================
            Ready button
        =========================================================== -->
        <Frame type="Frame" name="AnchoredPopupAnchorFrame">
            <Anchor side="Top" relative="$this" pos="Max" offset="0"/>
            <Anchor side="Left" relative="$parent/ReadyButton" pos="Mid" offset="-8"/>
            <Anchor side="Right" relative="$parent/ReadyButton" pos="Mid" offset="12"/>
            <Anchor side="Bottom" relative="$parent/ReadyButton" pos="Min" offset="0"/>
            <Width val="20"/>
            <Height val="20"/>
            <TooltipAnchorPosition val="TopCenter"/>
            <Frame type="AnchoredPopup" name="ReadyRequirementsPopup" template="HeroUnrankedDraftPanel/UnrankedDraftReadyButtonAnchoredPopupTemplate"/>
        </Frame>

        <Frame type="Button" name="ReadyButton" template="StandardTemplates/ReadyButtonTemplate_Middle">
            <Handle val="HeroUnrankedDraftPanel_ReadyButton"/>
            <ClickSound val="@UI_BNet_Ready"/>
            <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="-20"/>
            <Text val="@UI/ScreenHeroMatchmaking/ReadyButton"/>
            <TooltipAnchorPosition val="TopCenter"/>
        </Frame>
    </Frame>
</Desc>
