<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Desc>
    <DescFlags val="Locked"/>
    <!--
        To use this template, make sure that your class exposes the following properties:

            1. Rank - The numeric rank (or division)
            2. RankType - The rank type (gold, silver, etc)
            3. LeagueType - The type of the league (hero or team league)
    -->

    <!--
        Base Ranked Badge Template
    -->
    <Frame type="Frame" name="RankBadgeTemplate">
        <!--
            Sizing
        -->
        <Width val="350" />
        <Height val="375" />

        <!--
            Anchoring
        -->
        <Anchor side="Top" relative="$parent" pos="Min" offset="0" />
        <Anchor side="Left" relative="$parent" pos="Mid" offset="0" />
        <Anchor side="Right" relative="$parent" pos="Mid" offset="0" />

        <!--
            Frames
        -->
        <Frame type="Frame" name="ProgressBackground" template="StandardTemplates/ScoreProgressBackgroundTemplate">
            <Anchor side="Top" relative="$parent" pos="Min" offset="0" />
            <Anchor side="Left" relative="$parent" pos="Mid" offset="0" />
            <Anchor side="Right" relative="$parent" pos="Mid" offset="0" />
        </Frame>

        <Frame type="ProgressBar" name="ProgressBar" template="StandardTemplates/ScoreProgressBarTemplate">
            <Anchor side="Bottom" relative="$parent/ProgressBackground" pos="Max" offset="-23"/>
            <Anchor side="Left" relative="$parent/ProgressBackground" pos="Mid" offset="-1"/>
            <Anchor side="Right" relative="$parent/ProgressBackground" pos="Mid" offset="-1"/>
            <Width val="324"/>
            <Height val="312"/>
            <AcceptsMouseTooltip val="true" />
            <TooltipAnchorPosition val="TopCenter"/>

            <Frame type="Frame" name="FillImageContainer">
                <Frame type="Image" name="FillImage">
                    <Texture val="@UI/Storm_UI_Scorescreen_Ranked_Bar_Hero"/>
                </Frame>
            </Frame>
        </Frame>

        <Frame type="Frame" name="RankInformationContainer">
            <Anchor side="Left" relative="$parent" pos="Min" offset="2"/>
            <Anchor side="Top" relative="$parent" pos="Min" offset="0"/>
            <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
            <Anchor side="Bottom" relative="$parent" pos="Max" offset="0"/>

            <Frame type="Label" name="RankLabel">
                <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
                <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
                <Anchor side="Bottom" relative="$parent/NumericRankLabel" pos="Min" offset="18"/>
                <Style val="ScoreRankDescription"/>
                <Text val="@UI/RankedPlay/Rank"/>
            </Frame>

            <Frame type="Label" name="NumericRankLabel">
                <Anchor side="Top" relative="$parent" pos="Mid" offset="-1"/>
                <Anchor side="Bottom" relative="$parent" pos="Mid" offset="-1"/>
                <Anchor side="Left" relative="$parent" pos="Mid" offset="0"/>
                <Anchor side="Right" relative="$parent" pos="Mid" offset="0"/>
                <Style val="ScoreRankLevel"/>
            </Frame>
        </Frame>

        <Frame type="Frame" name="FleurishContainer" template="StandardTemplates/FleurishContainerTemplate">
            <Anchor side="Bottom" relative="$parent/ProgressBackground" pos="Max" offset="37"/>
            <Anchor side="Left" relative="$parent/ProgressBackground" pos="Min" offset="15"/>
        </Frame>
    </Frame>

    <!--
        Preseason - Hero League Ranked Badge
    -->
    <Frame type="Frame" name="HeroRankBadgeTemplate" template="HeroRankedLeagueBadge/RankBadgeTemplate">
        <Visible val="False" />
        <!-- <QueueTag val="RnkS"/> -->
        <Animation name="LeagueType">
            <Event event="OnShown" action="Play"/>
            <Controller type="Event" end="Stop">
                <Key type="Event" time="0" event="HeroLeague"/>
            </Controller>
        </Animation>
    </Frame>

    <!--
        Preseason - Team League Ranked Badge
    -->
    <Frame type="Frame" name="TeamRankBadgeTemplate" template="HeroRankedLeagueBadge/RankBadgeTemplate">
        <Frame type="ProgressBar" name="ProgressBar">
            <Frame type="Frame" name="FillImageContainer">
                <Frame type="Image" name="FillImage">
                    <Texture val="@UI/Storm_UI_Scorescreen_Ranked_Bar_Team"/>
                </Frame>
            </Frame>
        </Frame>

        <Visible val="False" />
        <!-- <QueueTag val="RnkP"/> -->
        <Animation name="LeagueType">
            <Event event="OnShown" action="Play"/>
            <Controller type="Event" end="Stop">
                <Key type="Event" time="0" event="TeamLeague"/>
            </Controller>
        </Animation>
    </Frame>
</Desc>
