<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Desc>
    <DescFlags val="Locked"/>

    <Frame type="Frame" name="AnimationDebugPanel">
        <Anchor relative="$parent" offset="0"/>

        <Frame type="Image" name="Test1">
            <Anchor side="Top" relative="$parent" pos="Min" offset="100"/>
            <Anchor side="Left" relative="$parent" pos="Min" offset="25"/>
            <Anchor side="Bottom" relative="TestingFrame" pos="Max" offset="5"/>
            <Anchor side="Right" relative="TestingFrame" pos="200%" offset="0"/>

            <Texture val="@UI/Editor/BorderImage"/>
            <TextureType val="NineSlice"/>

            <Frame type="Label" name="TestLabel">
                <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                <Anchor side="Left" relative="$parent" pos="Min" offset="5"/>
                <Text val="Test 1: (Loop, PingPong, Reverse, Speed)"/>
            </Frame>

            <Frame type="Label" name="TestExpectedLabel">
                <Anchor side="Top" relative="$parent/TestingFrame" pos="Min" offset="5"/>
                <Anchor side="Left" relative="$parent/TestingFrame" pos="Max" offset="5"/>
                <Anchor side="Right" relative="$parent" pos="Max" offset="-5"/>
                <Text val="Expected: A red dot moving clockwise in a circle.  A twice-as-fast, blue dot moving counter-clockwise in a circle."/>
            </Frame>

            <Frame type="Image" name="TestingFrame">
                <Anchor side="Top" relative="$parent/TestLabel" pos="Max" offset="5"/>
                <Anchor side="Left" relative="$parent" pos="Min" offset="5"/>
                <Width val="256"/>
                <Height val="256"/>

                <LayerColor val="7F7F7F" layer="0"/>

                <Frame type="Image" name="MovingImage">
                    <Anchor side="Top" relative="$parent" pos="Mid" offset="-8"/>
                    <Anchor side="Left" relative="$parent" pos="Mid" offset="-8"/>
                    <Width val="16"/>
                    <Height val="16"/>

                    <LayerColor val="FF0000" layer="0"/>
                    <Texture val="@UI/StormHeroConsoleCircleButtonMask" layer="0"/>

                    <Animation name="CircleTest">
                        <Event event="OnShown" action="Restart,Play"/>

                        <Controller type="Anchor" end="Loop" side="Left" relative="true">
                            <Key type="Curve" time="0" value="0" inout="Smooth"/>
                            <Key type="Curve" time="1" value="85" inout="Smooth"/>
                            <Key type="Curve" time="2" value="120" inout="Smooth"/>
                            <Key type="Curve" time="3" value="85" inout="Smooth"/>
                            <Key type="Curve" time="4" value="0" inout="Smooth"/>
                            <Key type="Curve" time="5" value="-85" inout="Smooth"/>
                            <Key type="Curve" time="6" value="-120" inout="Smooth"/>
                            <Key type="Curve" time="7" value="-85" inout="Smooth"/>
                            <Key type="Curve" time="8" value="0" inout="Smooth"/>
                        </Controller>
                        <Controller type="Anchor" end="PingPong" side="Top" relative="true">
                            <Key type="Curve" time="0" value="-120" inout="Smooth"/>
                            <Key type="Curve" time="1" value="-85" inout="Smooth"/>
                            <Key type="Curve" time="2" value="0" inout="Smooth"/>
                            <Key type="Curve" time="3" value="85" inout="Smooth"/>
                            <Key type="Curve" time="4" value="120" inout="Smooth"/>
                        </Controller>
                    </Animation>
                </Frame>
            </Frame>

            <Frame type="Image" name="TestingFrame2" template="StandardAnimationTemplates/AnimationDebugPanel/Test1/TestingFrame">
                <LayerAlpha val="0" layer="0"/>

                <Frame type="Image" name="MovingImage">
                    <LayerColor val="0000FF" layer="0"/>

                    <Animation name="CircleTest" speed="2">
                        <Event event="OnShown" action="DirectionReverse"/>
                    </Animation>
                </Frame>
            </Frame>
        </Frame>

        <Frame type="Image" name="Test2">
            <Anchor side="Top" relative="$parent/Test1" pos="Min" offset="0"/>
            <Anchor side="Left" relative="$parent/Test1" pos="Max" offset="5"/>
            <Anchor side="Bottom" relative="ButtonForward" pos="Max" offset="5"/>
            <Anchor side="Right" relative="TestingFrame" pos="200%" offset="0"/>

            <Texture val="@UI/Editor/BorderImage"/>
            <TextureType val="NineSlice"/>

            <Frame type="Label" name="TestLabel">
                <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                <Anchor side="Left" relative="$parent" pos="Min" offset="5"/>
                <Text val="Test 2: (DirectionForward, DirectionReverse, Curves, NonCurves)"/>
            </Frame>

            <Frame type="Label" name="TestExpectedLabel">
                <Anchor side="Top" relative="$parent/TestingFrame" pos="Min" offset="5"/>
                <Anchor side="Left" relative="$parent/TestingFrame" pos="Max" offset="5"/>
                <Anchor side="Right" relative="$parent" pos="Max" offset="-5"/>
                <Text val="Expected: The box should slide over the tick marks and rotate once per tick mark.  Playing forward at the end or reverse at the beginning should do nothing.  Playing the opposite direction while moving should change direction in place."/>
            </Frame>

            <Frame type="Image" name="TestingFrame">
                <Anchor side="Top" relative="$parent/TestLabel" pos="Max" offset="5"/>
                <Anchor side="Left" relative="$parent" pos="Min" offset="5"/>
                <Anchor side="Bottom" relative="$parent/TestExpectedLabel" pos="Max" offset="0"/>
                <Width val="256"/>

                <Texture val="@UI/Editor/BorderImage"/>
                <TextureType val="NineSlice"/>

                <Frame type="Image" name="Tick0">
                    <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                    <Anchor side="Left" relative="$parent" pos="0%" offset="-2"/>
                    <Width val="4"/>
                    <Height val="32"/>
                </Frame>
                <Frame type="Image" name="Tick1">
                    <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                    <Anchor side="Left" relative="$parent" pos="25%" offset="-2"/>
                    <Width val="4"/>
                    <Height val="32"/>
                </Frame>
                <Frame type="Image" name="Tick2">
                    <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                    <Anchor side="Left" relative="$parent" pos="50%" offset="-2"/>
                    <Width val="4"/>
                    <Height val="32"/>
                </Frame>
                <Frame type="Image" name="Tick3">
                    <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                    <Anchor side="Left" relative="$parent" pos="75%" offset="-2"/>
                    <Width val="4"/>
                    <Height val="32"/>
                </Frame>
                <Frame type="Image" name="Tick4">
                    <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                    <Anchor side="Left" relative="$parent" pos="100%" offset="-2"/>
                    <Width val="4"/>
                    <Height val="32"/>
                </Frame>

                <Frame type="Image" name="MovingImage">
                    <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                    <Anchor side="Left" relative="$parent" pos="Min" offset="-16"/>
                    <Width val="32"/>
                    <Height val="32"/>

                    <RotationPosition val="MiddleCenter"/>
                    <LayerColor val="FF0000" layer="0"/>

                    <Animation name="TimeLineTest">
                        <Event event="CommandForward" action="DirectionForward,Play" frame="$parent"/>
                        <Event event="CommandReverse" action="DirectionReverse,Play" frame="$parent"/>

                        <Controller type="Anchor" end="Pause" side="Left" relative="true">
                            <Key type="Curve" time="0" value="0" inout="Smooth"/>
                            <Key type="Curve" time="1" value="64" inout="Smooth"/>
                            <Key type="Curve" time="2" value="128" inout="Smooth"/>
                            <Key type="Curve" time="3" value="192" inout="Smooth"/>
                            <Key type="Curve" time="4" value="256" inout="Smooth"/>
                        </Controller>
                        <Controller type="Animation" end="Pause" animation="TimeLineTickTest">
                            <Key type="Animation" time="0" action="Play"/>
                            <Key type="Animation" time="1" action="Play"/>
                            <Key type="Animation" time="2" action="Play"/>
                            <Key type="Animation" time="3" action="Play"/>
                            <Key type="Animation" time="4" action="Play"/>
                        </Controller>
                    </Animation>

                    <Animation name="TimeLineTickTest">
                        <Controller type="Rotation" relative="False" end="Stop" >
                            <Key type="Curve" time="0" value="0" inout="linear"/>
                            <Key type="Curve" time=".4" value="360" inout="linear"/>
                        </Controller>
                    </Animation>
                </Frame>
            </Frame>

            <Frame type="Button" name="ButtonForward" template="StandardTemplates/StandardButtonMiniTemplate">
                <Anchor side="Left" relative="$parent" pos="Min" offset="5"/>
                <Anchor side="Top" relative="$parent/TestingFrame" pos="Max" offset="5"/>
                <Height val="80"/>

                <Text val="Play Forward"/>

                <Animation name="Click">
                    <Event event="OnMouseDown" action="Restart,Play"/>
                    <Controller type="Event" end="Stop" frame="$parent/TestingFrame">
                        <Key type="Event" time="0" event="CommandForward"/>
                    </Controller>
                </Animation>
            </Frame>

            <Frame type="Button" name="ButtonReverse" template="StandardTemplates/StandardButtonMiniTemplate">
                <Anchor side="Right" relative="$parent" pos="Max" offset="-5"/>
                <Anchor side="Top" relative="$parent/TestingFrame" pos="Max" offset="5"/>
                <Height val="80"/>

                <Text val="Play Reverse"/>

                <Animation name="Click">
                    <Event event="OnMouseDown" action="Restart,Play"/>
                    <Controller type="Event" end="Stop" frame="$parent/TestingFrame">
                        <Key type="Event" time="0" event="CommandReverse"/>
                    </Controller>
                </Animation>
            </Frame>

        </Frame>

        <Frame type="Image" name="Test3">
            <Anchor side="Top" relative="$parent/Test1" pos="Max" offset="5"/>
            <Anchor side="Left" relative="$parent/Test1" pos="Min" offset="0"/>
            <Anchor side="Bottom" relative="TestingFrame" pos="Max" offset="5"/>
            <Anchor side="Right" relative="TestingFrame" pos="200%" offset="0"/>

            <Texture val="@UI/Editor/BorderImage"/>
            <TextureType val="NineSlice"/>

            <Frame type="Label" name="TestLabel">
                <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                <Anchor side="Left" relative="$parent" pos="Min" offset="5"/>
                <Text val="Empty Test"/>
            </Frame>

            <Frame type="Label" name="TestExpectedLabel">
                <Anchor side="Top" relative="$parent/TestingFrame" pos="Min" offset="5"/>
                <Anchor side="Left" relative="$parent/TestingFrame" pos="Max" offset="5"/>
                <Anchor side="Right" relative="$parent" pos="Max" offset="-5"/>
                <Text val="Expected: Empty Test"/>
            </Frame>

            <Frame type="Image" name="TestingFrame">
                <Anchor side="Top" relative="$parent/TestLabel" pos="Max" offset="5"/>
                <Anchor side="Left" relative="$parent" pos="Min" offset="5"/>
                <Width val="256"/>
                <Height val="256"/>

                <Texture val="@UI/Editor/BorderImage"/>
                <TextureType val="NineSlice"/>
            </Frame>

        </Frame>

        <Frame type="Image" name="Test4">
            <Anchor side="Top" relative="$parent/Test1" pos="Max" offset="5"/>
            <Anchor side="Left" relative="$parent/Test3" pos="Max" offset="5"/>
            <Anchor side="Bottom" relative="TestingFrame" pos="Max" offset="5"/>
            <Anchor side="Right" relative="TestingFrame" pos="200%" offset="0"/>

            <Texture val="@UI/Editor/BorderImage"/>
            <TextureType val="NineSlice"/>

            <Frame type="Label" name="TestLabel">
                <Anchor side="Top" relative="$parent" pos="Min" offset="5"/>
                <Anchor side="Left" relative="$parent" pos="Min" offset="5"/>
                <Text val="Cutscene Attachment Test"/>
            </Frame>

            <Frame type="Label" name="TestExpectedLabel">
                <Anchor side="Top" relative="$parent/TestingFrame" pos="Min" offset="5"/>
                <Anchor side="Left" relative="$parent/TestingFrame" pos="Max" offset="5"/>
                <Anchor side="Right" relative="$parent" pos="Max" offset="-5"/>
                <Text val="Expected: A small checkered sphere moving in a circle, with a red text label following it (just above) that reads 'Test'."/>
            </Frame>

            <Frame type="Image" name="TestingFrame">
                <Anchor side="Top" relative="$parent/TestLabel" pos="Max" offset="5"/>
                <Anchor side="Left" relative="$parent" pos="Min" offset="5"/>
                <Width val="256"/>
                <Height val="256"/>

                <Texture val="@UI/Editor/BorderImage"/>
                <TextureType val="NineSlice"/>

                <Frame type="CutsceneFrame" name="AttachCutscene">
                    <Anchor relative="$parent"/>
                    <Width val="256"/>
                    <Height val="256"/>
                    <AutoPlay val="true"/>
                    <File val="Cutscenes/UI_AttachPointTest.StormCutscene"/>

                    <AttachPoint frame="OriginAnchorFrame" finderTag="attach" attachment="Ref_Center"/>

                    <Frame type="Frame" name="OriginAnchorFrame" template="StandardTemplates/CutsceneAttachPointTemplate"/>
                </Frame>

                <Frame type="Label" name="TestLabel">
                    <Anchor side="Top" relative="$parent/AttachCutscene/OriginAnchorFrame" pos="Mid" offset="-25"/>
                    <Anchor side="Left" relative="$parent/AttachCutscene/OriginAnchorFrame" pos="Mid" offset="0"/>
                    <Anchor side="Right" relative="$parent/AttachCutscene/OriginAnchorFrame" pos="Mid" offset="0"/>
                    <Anchor side="Bottom" relative="$parent/AttachCutscene/OriginAnchorFrame" pos="Mid" offset="-25"/>

                    <Style val="HeroSelectedNotificationErrorLabel"/>
                    <Text val="Test"/>
                </Frame>
            </Frame>
        </Frame>

    </Frame>

    <Animation name="ShakeAnimation">
        <Event event="StartShaking" action="RefreshBaseValue,Restart,Play"/>
        <Event event="StopShaking" action="Stop"/>
        <Event event="OnHidden" action="Stop"/>

        <Controller type="Anchor" end="Loop" side="Top" relative="true">
            <Key type="Curve" time="0" value="0"/>
            <Key type="Curve" time="0.04" value="2" in="Fast"/>
        </Controller>
        <Controller type="Anchor" end="Loop" side="Bottom" relative="true">
            <Key type="Curve" time="0" value="0"/>
            <Key type="Curve" time="0.04" value="2" in="Fast"/>
        </Controller>
        <Controller type="Anchor" end="Loop" side="Left" relative="true">
            <Key type="Curve" time="0" value="0"/>
            <Key type="Curve" time="0.05" value="-2" in="Fast"/>
        </Controller>
        <Controller type="Anchor" end="Loop" side="Right" relative="true">
            <Key type="Curve" time="0" value="0"/>
            <Key type="Curve" time="0.05" value="-2" in="Fast"/>
        </Controller>
    </Animation>

    <!-- Glow Vfx templates -->
    <Frame type="Image" name="GlowValueProviderTemplate">
        <Anchor relative="$parent"/>
        <Visible val="False"/>
        <Alpha val="0"/>
        <AdjustmentColor val="00ffffff"/>
    </Frame>

    <Frame type="Image" name="AddGlowOverride">
        <Frame type="Image" name="FillImage">
            <Anchor relative="$parent"/>
            <Texture val="{$parent/@Texture}"/>
            <BlendMode val="Add"/>
            <ColorAdjustMode val="AddSelf"/>
            <AdjustmentColor val="ffffff"/>
            <VisibilityOnFade val="True"/>
            <Alpha val="0"/>
        </Frame>
    </Frame>
</Desc>
